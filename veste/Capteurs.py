import numpy as np
import quaternion

H = quaternion.quaternion


class MesureCapteur():
    """Class abstraite représentant une mesure
    """

    def __init__(self, base: H = quaternion.quaternion(0, 0, 0, 0)) -> None:
        raise NotImplementedError

    @property
    def x(self) -> float:
        """Composante en x de la mesure
        """
        raise NotImplementedError

    @property
    def y(self) -> float:
        """Composante en x de la mesure
        """
        raise NotImplementedError

    @property
    def z(self) -> float:
        """Composante en x de la mesure
        """
        raise NotImplementedError

    def __abs__(self) -> float:
        return (self.x**2 + self.y**2 + self.z**2) ** (1/2)
