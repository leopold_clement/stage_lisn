import numpy as np
import scipy.optimize as opt
import veste.Capteurs as Capteurs
import quaternionic

theta = np.deg2rad(60)
"""Inclinaison du champs magnétique terrestre, en radiant"""
m = 0.5
"""Intensité du champs magnétique terreste, en Gauss"""
h_ref = m*np.array([np.cos(theta), 0, np.sin(theta)])
"""Champs magnétique de référence"""
g_ref = 9.81 * np.array([0, 0, 1])
"""Champs magnétique de référence"""


class ComplementaryObserver():
    """Observateur complementaire"""
    q: quaternionic.quaternion
    """Etat actuel du système"""

    def __init__(self, q_init: quaternionic.quaternion) -> None:
        self.q = q_init

    def input(self, acc: Capteurs.AccelerometterMesure, gyro: Capteurs.GyroscopeMesure, mag: Capteurs.MagnetoometterMesure):
        """Nourie l'observateur avec un nouveau point de donnée et renvoie la nnouvelle estimation

        :param acc: Acceleration mesurer
        :type acc: Capteurs.AccelerometterMesure
        :param gyro: Rotation mesurer
        :type gyro: Capteurs.GyroscopeMesure
        :param mag: Champs magnétique mesurer
        :type mag: Capteurs.MagnetoometterMesure
        :return: Nouvelle estimation de rotation
        :rtype: quaternion.quaternion
        """

        # Calcul de q_m
        def err(q):
            q_fmn = q * acc.quaternion_zero * q**(-1)
            q_hmn = q * mag.quaternion_zero * q**(-1)
            err = np.concatenate([q_fmn.vector(), q_hmn.vector()])
            return err
        res = opt.least_squares(err, self.q, method='lm')
        q_m = res.x

        q_n = q_m # ou pas
        # Complementary observer
        dot_q_n = 1/2 * \
            np.concatenate(
                [-q_n.vector(), q_n.w*np.eye(3) + q_n.skew()]) * gyro.vec

        return self.q
