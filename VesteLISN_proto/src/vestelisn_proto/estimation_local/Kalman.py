from abc import ABC, abstractmethod, abstractproperty
from functools import cache
from typing import Callable, List, Self, Tuple

import numpy as np
import sympy as s
from vestelisn_proto.utils.physique import Referentiel
from vestelisn_proto.utils.quaternions import Quaternion
from vestelisn_proto.utils.type_alias import (FullMesureVector, MesureVector,
                                              StateVector)

from . import GenericEstimator

ref_terre_paris = Referentiel()


def extract_quaternion(q: s.algebras.Quaternion) -> List[float]:
    return [q.a, q.b, q.c, q.d]


class ExtendedKalmanFilter(ABC):
    """Filtre de Kalman étendu, non-linéaire
    """

    def __init__(self, n_etat: int, n_observation: int, P: np.ndarray, Q: np.ndarray, R: np.ndarray) -> None:
        self._x = np.zeros((n_etat,), dtype=np.float64)
        if P.shape != (n_etat, n_etat):
            raise ValueError
        if Q.shape != (n_etat, n_etat):
            raise ValueError
        if R.shape != (n_observation, n_observation):
            raise ValueError
        self._P = P
        self._Q = Q
        self._R = R

    @property
    def x(self) -> np.ndarray:
        """Etat estimé du filtre

        :return: :math;`x_{k | k}`
        :rtype: np.ndarray
        """
        return self._x

    @x.setter
    def x(self, v: np.ndarray):
        if v.shape == self._x.shape:
            self._x = v
            return
        raise ValueError

    @property
    def P(self) -> np.ndarray:
        """Covariance de l'incertitude sur l'état

        :return: :math:`P_{k|k}`
        :rtype: np.ndarray
        """
        return self._P

    @P.setter
    def P(self, v: np.ndarray):
        if v.shape == self._P.shape:
            self._P = v
            return
        raise ValueError

    @property
    def Q(self) -> np.ndarray:
        """Matrice de covarrience du bruit de modèle

        :return: Q
        :rtype: np.ndarray
        """
        return self._Q

    @Q.setter
    def Q(self, Q: np.ndarray):
        if self.Q.shape != Q.shape:
            raise ValueError
        self._Q = Q

    @property
    def R(self) -> np.ndarray:
        """Matrice de covarrience du bruit de mesure

        :return: R
        :rtype: np.ndarray
        """
        return self._R

    @R.setter
    def R(self, R: np.ndarray):
        if self.R.shape != R.shape:
            raise ValueError
        self._R = R

    @abstractproperty
    def f(self) -> Callable[[np.ndarray], np.ndarray]:
        """Fonction de transition du système

        :math:`x_{k} = f(x_{k-1})`

        :return: f(x: np.array)
        :rtype: Callable[[np.array], np.ndarray]
        """
        ...

    @abstractproperty
    def F(self) -> np.ndarray:
        """Matrice Jacobienne de f.

        :math:`F_k = \\frac{\\partial f}{\\partial x}`

        :return: :math:`F_k`
        :rtype: np.ndarray
        """
        ...

    @abstractproperty
    def h(self) -> Callable[[np.ndarray], np.ndarray]:
        """Fonction d'observation du système

        :math:`z_{k} = h(x_{k})`

        :return: h(x: np.array)
        :rtype: Callable[[np.array], np.ndarray]
        """
        ...

    @abstractproperty
    def H(self) -> np.ndarray:
        """Matrice Jacobienne de f.

        :math:`H_k = \\frac{\\partial H}{\\partial x}`

        :return: :math:`H_k`
        :rtype: np.ndarray
        """
        ...

    def predict(self) -> Tuple[np.ndarray, np.ndarray]:
        """Predict the next state of the système

        :return: :math:`x_{k | k-1} , P_{k | k-1}`
        :rtype: Tuple[np.ndarray, np.ndarray]
        """
        hat_x = self.f(self.x)
        hat_P = self.F @ self.P @ self.F.T + self.Q
        return (hat_x, hat_P)

    def update(self, z: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        """Update le filtre avec une nouvelle mesure

        :param z: :math:`z_k`
        :type z: np.ndarray
        :return:  :math:`x_{k | k} , P_{k | k}`
        :rtype: Tuple[np.ndarray, np.ndarray]
        """
        hat_x, hat_P = self.predict()
        y = z - self.h(hat_x)
        S = self.H @ hat_P @ self.H.T + self.R
        K = hat_P @ self.H.T @ np.linalg.inv(S)
        x = hat_x + K @ y
        P = (np.eye(x.shape[0]) - K @ self.H) @ hat_P
        self.x = x
        self._P = P
        return (x, P)


class ExtendedKalmanFilterAttitude(ExtendedKalmanFilter):
    def __init__(self, n_observation: int, P: np.ndarray, Q: np.ndarray, R: np.ndarray, delta_t: float = 0.01, ref: Referentiel = ref_terre_paris) -> None:
        super().__init__(7, n_observation, P, Q, R)
        self._x = np.array([0, 0, 0, 1.0, 0, 0, 0])
        self._ref = ref
        self._delta_t = delta_t

    @property
    def x(self) -> StateVector:
        return self._x

    @x.setter
    def x(self, v: StateVector):
        if v.shape == self._x.shape:
            q = Quaternion(*v[3:])
            v[3:] = q.unit.array
            self._x = v
            return
        raise ValueError

    @staticmethod
    def symbolic_expretion_substitution(func: s.Matrix, x: StateVector) -> StateVector:
        """Remplace le vecteur d'état du filtre dans une expresion symbolique

        :param func: expression symbolique de "x:7"
        :type func: s.Matrix
        :param x: Vecteur d'état à remplacer
        :type x: StateVector
        :return: Valeur de l'expression au point donné
        :rtype: StateVector
        """
        for sim, val in zip(s.symbols("x:7"), x):
            func = func.subs(sim, val)
        return np.array([float(v) for v in func])

    @staticmethod
    def derive_substitute_matrix(expr: s.Matrix, x: StateVector) -> np.ndarray:
        """Dérive l'expression symbolique `f` et remplace les varriable `x_i` par x

        :param f: Expression à derrivé
        :type f: s.Matrix
        :param x: Vecteur d'état
        :type x: StateVector
        :return: Matrice jacobienne au point décrit
        :rtype: np.ndarray
        """
        F = s.derive_by_array(expr, s.symbols('x:7'))
        for sym, val in zip(s.symbols("x:7"), x):
            F = F.subs(sym, val)
        a = [[float(x[0]) for x in c] for c in F]
        return np.array(a).T

    @property
    def f_sim(self) -> s.Matrix:
        """Expression sympolique representant la valeur du prochain 

        :return: :math:`f : x_{k-1} -> x_{k}`
        :rtype: s.Matrix
        """
        dt = s.symbols("delta_t")
        tau = s.symbols("tau")
        xs = s.symbols("x:7")
        omega = s.Matrix(xs[:3])
        w = s.algebras.Quaternion(0, *omega)
        next_omega = omega - tau * omega
        q = s.algebras.Quaternion(*xs[3:])
        dot_q = 1/2*q*w * dt
        next_q = (q * dot_q) #.normalize()
        next_x = s.factor(
            s.Matrix(list(next_omega)+extract_quaternion(next_q)))
        return next_x

    @property
    def f(self) -> Callable[[StateVector], StateVector]:
        """Fonction de transition du système

        :math:`x_{k} = f(x_{k-1})`

        :return: f(x: StateVector)
        :rtype: Callable[[StateVector], StateVector]
        """
        return lambda x: self.symbolic_expretion_substitution(self.f_sim, x)

    @property
    def h_sim(self) -> s.Matrix:
        xs = s.symbols("x:7")
        w = s.algebras.Quaternion(0, *xs[:3])
        q = s.algebras.Quaternion(*xs[3:])
        f_ref = self._ref.vect_f_sim()
        m_ref = self._ref.vect_mag_sim()
        q_f_B = s.algebras.Quaternion(0, * f_ref)
        q_mag_B = s.algebras.Quaternion(0, * m_ref)
        q_f_N = q*q_f_B*q.conjugate()
        q_mag_N = q*q_mag_B*q.conjugate()
        return s.factor(s.Matrix(extract_quaternion(q_f_N)[1:] + extract_quaternion(w)[1:] + extract_quaternion(q_mag_N)[1:]))

    @property
    def h(self) -> Callable[[StateVector], FullMesureVector]:
        """Fonction d'observation du système

        :math:`z_{k} = h(x_{k})`

        :return: h(x: StateVector)
        :rtype: Callable[[StateVector], FullMesureVector]
        """
        return lambda x: self.symbolic_expretion_substitution(self.h_sim, x)

    @property
    def delta_t(self) -> float:
        return self._delta_t


class NaiveKalmanFilterEstimator(ExtendedKalmanFilterAttitude, GenericEstimator.Estimator9DOF):
    """Naive Kalman filter with 9 observation
    """

    def __init__(self, P: np.ndarray, Q: np.ndarray, R: np.ndarray) -> None:
        super().__init__(9, P, Q, R)
        self._x[3] = 1

    @property
    def F(self) -> np.ndarray:
        """Matrice Jacobienne de f.

        :math:`F_k = \\frac{\\partial f}{\\partial x}`

        :return: :math:`F_k`
        :rtype: np.ndarray
        """
        f = self.f_sim
        return self.derive_substitute_matrix(f, self.x)

    @property
    def H(self) -> np.ndarray:
        """Matrice Jacobienne de f.

        :math:`H_k = \\frac{\\partial H}{\\partial x}`

        :return: :math:`H_k`
        :rtype: np.ndarray
        """
        return self.derive_substitute_matrix(self.h_sim, self.x)

    @property
    def estimation(self) -> StateVector:
        return self.x

    def feed(self, mesure: FullMesureVector) -> StateVector:
        x, P = self.update(mesure)
        return x

    def set_state(self, x: StateVector):
        self.x = x


class ReducedKalmanFilter(ExtendedKalmanFilterAttitude):
    """_summary_

    Design, Implementation, and Experimental Results of a Quaternion-Based Kalman Filter for Human Body Motion Tracking Xiaoping Yun, Fellow, IEEE, and Eric R. Bachmann, Member, IEEE
    """

    def __init__(self, P: np.ndarray, Q: np.ndarray, R: np.ndarray, ref: Referentiel = ref_terre_paris) -> None:
        super().__init__(7, P, Q, R)

    @property
    def h(self) -> Callable[[np.ndarray], np.ndarray]:
        """Fonction d'observation du système

        :math:`z_{k} = h(x_{k})`

        :return: h(x: np.array)
        :rtype: Callable[[np.array], np.ndarray]
        """
        return lambda x: x

    @property
    def F(self) -> np.ndarray:
        """Matrice d'evolution du système, dépendant de l'état du système

        :return: F ou phi
        :rtype: np.ndarray
        """
        tau = np.array([0.1, 0.2, 0.3])
        w = np.diag(np.exp(self._delta_t*(1/tau)))
        top = np.concatenate([w, np.zeros((3, 4))], axis=1)
        x = self.x
        x = np.concatenate([np.zeros((1,)), x])
        d = self._delta_t/2
        bot = np.array(
            [[-x[5]*d, -x[6]*d, -x[7]*d,    1, -x[1]*d, -x[2]*d, -x[3]*d],
             [x[4]*d, -x[7]*d,  x[6]*d, x[1]*d,     1,  x[3]*d, -x[2]*d],
             [x[7]*d,  x[4]*d, -x[5]*d, x[2]*d, -x[3]*d,     1,  x[1]*d],
             [-x[6]*d,  x[5]*d,  x[4]*d, x[3]*d,  x[2]*d, -x[1]*d,     1]])
        return np.concatenate([top, bot], axis=0)

    @property
    def H(self) -> np.ndarray:
        return np.eye(7)


class StagedKalmanEstimator(GenericEstimator.Estimator9DOF):
    def __init__(self, kalman: ReducedKalmanFilter, wahba: GenericEstimator.WahbaEstimator) -> None:
        self._kalman = kalman
        self._wahba = wahba

    @property
    def estimation(self) -> StateVector:
        return self._kalman.x

    def feed(self, mesure: FullMesureVector) -> StateVector:
        q = self._wahba.estimate(Quaternion(0,
                                            *self._kalman.x[3:]), mesure[:3], mesure[-3:])
        x, P = self._kalman.update(np.concatenate([mesure[3:6], q.array]))
        return x

    def set_state(self, x: StateVector):
        self._kalman.x = x
