from abc import ABC, abstractmethod, abstractproperty
import numpy as np
from vestelisn_proto.utils.physique import Referentiel
from vestelisn_proto.utils.quaternions import Quaternion
from vestelisn_proto.utils.type_alias import StateVector, FullMesureVector, MesureVector
class WahbaEstimator(ABC):
    """Generique estimator of the attitude of a body from an accelerometre and a magnetometre
    """
    
    @abstractmethod
    def __init__(self, ref : Referentiel = Referentiel()) -> None:
        self.ref = ref

    @property
    def ref_f(self) -> MesureVector:
        """Reference acceleration
        """
        return self.ref.vect_f

    @property
    def ref_h(self) -> MesureVector:
        """Reference magnetic field"""
        return self.ref.vect_mag
    
    @abstractmethod
    def estimate(self, q_init: Quaternion, acc: MesureVector, mag: MesureVector)->Quaternion:
        ...

class Estimator9DOF(ABC):
    """Abstract class representing an attitude estimator with 9 degre of freedom

    the entry are :
    - acceleration
    - rotation speed
    - magnetic field

    the state vector is:
    - rotation speed
    - unit quaternion
    """
    @abstractproperty
    def estimation(self) -> StateVector:
        """Get the current estimation of the state

        :return: State vector of the body
        :rtype: StateVector
        """
        ...
    
    @abstractmethod
    def feed(self, mesure:FullMesureVector) -> StateVector:
        """Feed a new mesurement to the estimator

        :param mesure: Mesurement vector
        :type mesure: FullMesureVector
        :return: New state vector
        :rtype: StateVector
        """
        ...
    
    @abstractmethod
    def set_state(self, x: StateVector):
        """Set the estimated state

        :param x: State vector
        :type x: StateVector
        """
        ...