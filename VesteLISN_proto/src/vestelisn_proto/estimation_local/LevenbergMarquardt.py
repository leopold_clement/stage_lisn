import numpy as np
import scipy.optimize as opt

from vestelisn_proto.utils.type_alias import MesureVector
from vestelisn_proto.utils.quaternions import Quaternion
import vestelisn_proto.estimation_local.GenericEstimator as GenericEstimator

class LevenbergMarquardtEstimator(GenericEstimator.WahbaEstimator):
    """Etimateur de l'orientation d'un solide à partir de l'algorythme de LevenbergMarquard
    """

    def __init__(self) -> None:
        pass

    def estimate(self, q_init: Quaternion, acc: MesureVector, mag: MesureVector)->Quaternion:
        """Estimate the rotation of the device in the reference base

        :param q_init: Last known rotation
        :type q_init: Quaternion
        :param acc: Acceleration mesured by the device
        :type acc: MesureVector
        :param mag: Magnetic field mesured by the device
        :type mag: MesureVector
        :return: Estimation of the rotation of the device
        :rtype: Quaternion
        """
        def err(qn: np.ndarray):
            q = Quaternion(*qn)
            q_fmn = q @ Quaternion(0, * acc) @ q.conjugate
            q_hmn = q @ Quaternion(0, * mag) @ q.conjugate
            err_f = self.ref_f - q_fmn.vect
            err_g = self.ref_h - q_hmn.vect
            err = np.concatenate([err_f, err_g])
            return err
        res = opt.least_squares(err, q_init.array,  method='lm')
        q_m = Quaternion(* res.x)
        return q_m
    