from . import LevenbergMarquardt
from . import GenericEstimator
import numpy as np

from vestelisn_proto.utils.type_alias import MesureVector
from typing import List
Quaternion = LevenbergMarquardt.Quaternion


class ComplementaryObserver(GenericEstimator.Estimator9DOF):
    """Observateur complementaire pour l'estimation de l'orientation d'un solide

    https://hal.science/hal-00690145v2
    """

    def __init__(self, q_init: Quaternion, wahba: GenericEstimator.WahbaEstimator, k_weight: List[float], rho: float) -> None:
        self._hat_q = q_init
        self._wahba = wahba
        self._k = k_weight
        self._rho = rho

    @property
    def hat_q(self) -> Quaternion:
        """Get the current attitude estimation

        :return: Quaternion representing the estimation
        :rtype: Quaternion
        """
        return self._hat_q

    @property
    def K_1(self) -> np.ndarray:
        """Gain matrix one"""
        return np.diag([1.0] + self._k[:3])

    @property
    def K_2(self) -> np.ndarray:
        """Gain matrix two"""
        return np.diag([1.0] + self._k[3:])

    @property
    def rho(self) -> float:
        """Surface boundary layer"""
        return self._rho

    def update(self, acc: MesureVector, gyro: MesureVector, mag: MesureVector) -> Quaternion:
        q_m = self._wahba.estimate(self._hat_q, acc, mag)

        q_e = self.hat_q.conjugate @ q_m

        khi_1 = self.K_1 @ _sat(q_e.vect, self.rho)
        khi_2 = self.K_2 @ q_e.vect

        delta_K_1 = Quaternion(*khi_1 / np.linalg.norm(khi_1))
        delta_K_2 = Quaternion(*khi_2 / np.linalg.norm(khi_2))

        tmp = np.concatenate(
            [- self.hat_q.vect, np.eye(3) * self.hat_q.w + self.hat_q.skew], axis=1)

        dot_q = delta_K_1 @ delta_K_2 @ Quaternion(* 1/2 * tmp @ gyro)

        self._hat_q = (self.hat_q + dot_q).unit
        return self.hat_q


def _sat(vec: np.ndarray, rho: float) -> np.ndarray:  # TODO comprendre l'équation 10
    pos = vec > rho
    neg = vec < -rho
    mid = not (pos | neg)
    return mid*(vec/rho) + pos*np.ones_like(vec) - neg*np.ones_like(vec)
