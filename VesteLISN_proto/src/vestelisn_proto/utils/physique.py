import numpy as np
import sympy as s
from .type_alias import MesureVector

class Referentiel():
    """Referentiel de référence pour l'environement terrestre sans effet inertielle
    """
    def __init__(self, g: float = 9.81, m: float = 0.5, theta_mag: float = np.deg2rad(60)) -> None:
        """Referentiel de champs de pesanteur et magnétique

        :param g: intersité de la gravité, en m/s^2, defaults to 9.81
        :type g: float, optional
        :param m: intensité du champs magnétique, en Gauss, defaults to 0.5
        :type m: float, optional
        :param theta_mag: Agnle du champs magnétique, en radian, defaults to np.deg2rad(60)
        :type theta_mag: float, optional
        """
        self.g = g
        self.m = m
        self.theta_mag = theta_mag

    @property
    def vect_f(self) -> MesureVector:
        """Acceleration de la gravité au repos

        :return: Vecteur pesenteur au repos, dans le repère terrestre.
        :rtype: MesureVector
        """
        return np.array([0, 0, self.g])

    @property
    def vect_mag(self) -> MesureVector:
        """Champs magnétique par défault

        :return: Champs magnétique dans le repère terrestre.
        :rtype: MesureVector
        """
        return self.m * np.array([np.cos(self.theta_mag), 0, np.sin(self.theta_mag)])
    
    @staticmethod
    def vect_f_sim() -> s.Matrix:
        g = s.symbols("g")
        return s.Matrix([0, 0, g])
    
    @staticmethod
    def vect_mag_sim() -> s.Matrix:
        M, theta = s.symbols("M theta")
        return M * s.Matrix([s.cos(theta), 0, s.sin(theta)])