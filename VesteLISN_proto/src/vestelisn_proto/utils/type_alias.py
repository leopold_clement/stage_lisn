from typing import NewType
from nptyping import NDArray, Shape, Float32

StateVector = NewType('StateVector', NDArray[Shape['7'], Float32])
FullMesureVector = NewType('FullMesureVector', NDArray[Shape['9'], Float32])
MesureVector = NewType('MesureVector', NDArray[Shape['3'], Float32])
