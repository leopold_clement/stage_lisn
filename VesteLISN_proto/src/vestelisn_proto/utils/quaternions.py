from __future__ import annotations
from typing import Self, TypeVar
import numpy as np

class Quaternion():
    """Basic quaternion class
    """

    epsilon_equality = 0.0000001

    _val : np.ndarray

    def __init__(self, w: float, x: float = 0, y: float = 0, z: float = 0) -> None:
        self._val = np.array([w, x, y, z], dtype=np.float64)

    @property
    def w(self) -> float:
        return self._val[0]

    @property
    def i(self) -> float:
        return self._val[1]

    @property
    def j(self) -> float:
        return self._val[2]

    @property
    def k(self) -> float:
        return self._val[3]

    @property
    def vect(self) -> np.ndarray:
        """Return the imaginary part of the quaternion as an array

        :return: Imaginary part of the quaternion
        :rtype: np.ndarray
        """
        return self._val[1:]
    
    @property
    def array(self) -> np.ndarray:
        """Get the quaternion as a numpy array

        :return: Numpy array representing the quaternion
        :rtype: np.ndarray
        """
        return self._val

    @property
    def unit(self) -> Quaternion:
        """Get the normalized quaternion

        :return: Quaternion with length 1
        :rtype: Self
        """
        norm = abs(self)
        return self*(1/norm)

    @property
    def conjugate(self) -> Quaternion:
        """Get conjugate quaternion

        :return: conjugate quaternion
        :rtype: Self
        """
        return Quaternion(* self._val * [1, -1, -1, -1])

    @property
    def skew(self) -> np.ndarray:
        """Get the skew-symetric matrix of the quaternion

        :return: The skew-symetric matrix
        :rtype: np.ndarray
        """
        return np.array([[0, -self.k, self.j],
                         [self.k, 0, -self.i], 
                         [-self.j, self.i, 0]])

    def __repr__(self) -> str:
        return f'{self.w} + {self.i}*i + {self.j}*j + {self.k}*k'

    def __abs__(self) -> float:
        norm = sum([x**2 for x in self._val])
        return np.sqrt(norm)

    def __eq__(lhs, rhs: object) -> bool:

        if not isinstance(rhs, Quaternion):
            raise NotImplementedError
        return (lhs._val == rhs._val).all() | (abs(lhs-rhs) <= Quaternion.epsilon_equality*abs(lhs+rhs))

    def __mul__(self, __o: object) -> Quaternion:

        if isinstance(__o, float) or isinstance(__o, int):
            return Quaternion(*__o*self._val)
        raise NotImplementedError

    def __rmul__(self, __o: object)-> Quaternion:
        return self*__o

    def __add__(lhs, rhs: object)-> Quaternion:
        if isinstance(rhs, Quaternion):
            return Quaternion(*(lhs._val+rhs._val))
        if isinstance(rhs, float) or isinstance(rhs, int):
            return lhs+Quaternion(rhs)
        raise NotImplementedError

    def __sub__(lhs, rhs: object)-> Quaternion:
        if isinstance(rhs, Quaternion) or isinstance(rhs, float) or isinstance(rhs, int):
            return lhs + (-1 * rhs)
        raise NotImplementedError

    def __matmul__(lhs, rhs: object)-> Quaternion:
        if isinstance(rhs, Quaternion):
            return Quaternion(
                lhs.w*rhs.w - lhs.i*rhs.i - lhs.j*rhs.j - lhs.k*rhs.k,
                lhs.w*rhs.i + lhs.i*rhs.w + lhs.j*rhs.k - lhs.k*rhs.j,
                lhs.w*rhs.j + lhs.j*rhs.w + lhs.k*rhs.i - lhs.i*rhs.k,
                lhs.w*rhs.k + lhs.k*rhs.w + lhs.i*rhs.j - lhs.j*rhs.i)
        raise NotImplementedError