from vestelisn_proto.estimation_local.Kalman import NaiveKalmanFilterEstimator, ReducedKalmanFilter
import numpy as np

Q_def = np.eye(7)
P_def = np.zeros_like(Q_def)
R_def_9 = np.eye(9)
R_def_7 = np.eye(7)


class Test_Naive_Kalman():
    def test_non_virtuel(self):
        k = NaiveKalmanFilterEstimator(P=P_def, Q=Q_def, R=R_def_9)

    def test_fsim(self):
        k = NaiveKalmanFilterEstimator(P=P_def, Q=Q_def, R=R_def_9)
        k.f_sim

    def test_hsim(self):
        k = NaiveKalmanFilterEstimator(P=P_def, Q=Q_def, R=R_def_9)
        k.h_sim

    def test_f(self):
        k = NaiveKalmanFilterEstimator(P=P_def, Q=Q_def, R=R_def_9)
        assert k.f(k.x).shape == (7,)

    def test_h(self):
        k = NaiveKalmanFilterEstimator(P=P_def, Q=Q_def, R=R_def_9)
        assert k.h(k.x).shape == (9,)

    def test_F(self):
        k = NaiveKalmanFilterEstimator(P=P_def, Q=Q_def, R=R_def_9)
        assert k.F.shape == (7, 7)

    def test_H(self):
        k = NaiveKalmanFilterEstimator(P=P_def, Q=Q_def, R=R_def_9)
        assert k.H.shape == (9, 7)

    def test_PredictUpdate(self):
        k = NaiveKalmanFilterEstimator(P=P_def, Q=Q_def, R=R_def_9)
        k.predict()
        k.update(np.array([0, 0, 1, 0, 0, 0, 0.5, 0, 0.3]))

class Test_ReducedKalmanFilter():
    def test_non_virtuel(self):
        k = ReducedKalmanFilter(P=P_def, Q=Q_def, R=R_def_7)

    def test_f(self):
        k = ReducedKalmanFilter(P=P_def, Q=Q_def, R=R_def_7)
        assert k.f(k.x).shape == (7,)

    def test_h(self):
        k = ReducedKalmanFilter(P=P_def, Q=Q_def, R=R_def_7)
        assert k.h(k.x).shape == (7,)

    def test_F(self):
        k = ReducedKalmanFilter(P=P_def, Q=Q_def, R=R_def_7)
        assert k.F.shape == (7, 7)

    def test_H(self):
        k = ReducedKalmanFilter(P=P_def, Q=Q_def, R=R_def_7)
        assert k.H.shape == (7, 7)

    def test_PredictUpdate(self):
        k = ReducedKalmanFilter(P=P_def, Q=Q_def, R=R_def_7)
        k.predict()
        k.update(np.array([0, 0, 0, 1, 0, 0, 0]))