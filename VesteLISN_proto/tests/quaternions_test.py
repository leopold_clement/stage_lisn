from vestelisn_proto.utils.quaternions import Quaternion


def test_add():
    assert Quaternion(1, 0, 1, 0) + Quaternion(1, 1, -
                                               1, 1) == Quaternion(2, 1, 0, 1)


def test_mul_float():
    assert 2*Quaternion(0, 1, 2, 3) == Quaternion(0, 2, 4, 6)


def test_mul_quat():
    assert Quaternion(1, 0, 0, 0) @ Quaternion(1, 2,
                                               3, 4) == Quaternion(1, 2, 3, 4)
    assert Quaternion(0, 1, 0, 0) @ Quaternion(1, 2,
                                               3, 4) == Quaternion(-2, 1, -4, 3)
    assert Quaternion(1, 2, 3, 4) @ Quaternion(10, 20, 30,
                                               40) == Quaternion(-280, 40, 60, 80)


def test_unit():
    assert Quaternion(2, 0, 0, 0).unit == Quaternion(1, 0, 0, 0)


def test_conjugate():
    A = Quaternion(1, -4, 3, 325234).unit
    B = A.conjugate
    one = A@B
    assert one == Quaternion(1)
