from vestelisn_proto.estimation_local import LevenbergMarquardt as lm

def test_estimate():
    estimator = lm.LevenbergMarquardtEstimator()
    q1 = lm.Quaternion(1, 2, 3, 4).unit
    acc = lm.LevenbergMarquardtEstimator.ref_f
    mag = lm.LevenbergMarquardtEstimator.ref_h
    q2 = estimator.estimate(q1, acc, mag)
    assert q2 == lm.Quaternion(1, 0, 0, 0)