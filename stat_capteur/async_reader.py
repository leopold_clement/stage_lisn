from nicegui import ui
import asyncio
import serial
import time
from typing import List

new_data = asyncio.Queue()


async def port_puller(port: serial.Serial) -> str:
    line = ""
    while True:
        await asyncio.sleep(0.0001)
        b = port.read().decode()
        if len(b):
            if (b == "\n") and (line != ""):
                yield line
                line = ""
            else:
                line += b


async def port_reader(ser: serial.Serial) -> List[float]:
    async for line in port_puller(ser):
        datas_raw = line.split(",")
        try:
            data = [time.perf_counter()] + [float(x) for x in datas_raw]
            yield data
        except ValueError:
            print(line)

ui.label("Stage")
display = ui.label("valeur")
with ui.row():
    line_plots = [ui.line_plot(n=3, limit=500, figsize=(5, 3), update_every=25).with_legend(
        ['x', 'y', 'z'], loc='upper center', ncol=2) for _ in range(3)]
ser = serial.Serial("/dev/ttyACM1", 115200, timeout=0)


async def label_update():
    async for data in port_reader(ser):
        display.set_text(f"{data}")
        resolution = [1, 1, 1]
        for idx, plot in enumerate(line_plots):
            vect:List[float] = data[1 + 3*idx: 1 + 3*(idx+1)]
            points = [[x] for x in vect]
            # points += [[sum([x**2 for x in vect])**(1/2)]]
            # print(points)
            plot.push([data[0]], points)

ui.timer(1, label_update)
ui.run(reload=False)

ser.close()
