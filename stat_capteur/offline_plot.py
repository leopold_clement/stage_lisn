#! /bin/python3

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

names = ['acc', 'gyro', 'mag']
df = pd.read_csv('tmp/data_raw.csv')

fig, axs = plt.subplots(3, 1, sharex=True)

t = df['t'].to_numpy()
for ax, name_mesure in zip(axs, names):
    a = df[[f'{name_mesure}_x', f'{name_mesure}_y', f'{name_mesure}_z']].to_numpy()
    for idx, name_axe in enumerate(["x", "y", "z"]):
        ax.plot(t, a[:, idx], label=name_axe, alpha=0.7)
    squares = a**2
    sums = np.sum(squares, axis=1)
    norms = np.sqrt(sums)
    ax.plot(t, norms, "k", label="Norm", alpha = 0.9)
    ax.set_ylabel(name_mesure)
    ax.legend()
    ax.grid()
fig.tight_layout()
fig.savefig("tmp/temporel.png", dpi=1000)
