import serial.threaded as serial_thread
import serial
import queue
import threading
import time
from nicegui import ui


new_data = queue.Queue()

class QueueLines(serial_thread.LineReader):
    def connection_made(self, transport):
        super(QueueLines, self).connection_made(transport)
        print("Reader OK")

    def handle_line(self, line):
        print(f"got line {line}")
        try:
            datas_raw = line.split(",")
            data = [time.perf_counter()] + [float(x) for x in datas_raw]
            if len(data) != 10:
                raise ValueError
            new_data.put(data)
        except ValueError:
            print(line)

    def connection_lost(self, exc):
        if exc:
            print(exc)


ui.label("Data stage")
display = ui.label("")

line_plots = [ui.line_plot(n=3, limit=50, figsize=(5, 3), update_every=5).with_legend(
    ['x', 'y', 'z'], loc='upper center', ncol=2) for _ in range(3)]


def update_display():
    while not new_data.empty():
        data = new_data.get()
        display.set_text(display)
        for idx, plot in enumerate(line_plots):
            plot.push([data[0]], [[data[idx+1]], [data[idx+2]], [data[idx+3]]])

ui.timer(0.5, update_display)
ser = serial.Serial("/dev/ttyACM0", 115200)
t_serial = serial_thread.ReaderThread(ser, QueueLines)
t_serial.start()
print(t_serial.alive)

v = new_data.get()
print(v)
t_serial.join()
ui.run(reload=False)
