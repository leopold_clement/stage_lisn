#! /bin/python3

import pathlib
import csv
from tqdm import tqdm
import numpy as np
in_path = pathlib.Path('tmp/data_raw.csv')
out_path = pathlib.Path('tmp/data_clean.csv')



resolution = [np.deg2rad(1/16.384), np.deg2rad(1/131), 0.15 * 10**-6]

with open(in_path, 'r') as in_file, open(out_path, 'w') as out_file:
    headers = in_file.readline()    
    out_file.writelines([headers])
    for line in tqdm(in_file.readlines()):
        fields = line.split(',')
        if len(fields) != 10:
            print(line)
            continue
        for idx_m, fact in enumerate(resolution):
            for idx in range(3):
                i = 1 + 3*idx_m + idx
                fields[i] = str(fact * float(fields[i]))
        out_file.write(','.join(fields))
        out_file.write('\n')
