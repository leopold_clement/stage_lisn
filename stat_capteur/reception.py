import serial
import time
from tqdm import tqdm
import pathlib

file_path = pathlib.Path('tmp/data_raw.csv')
N_point = 1000
serial_path = "/dev/ttyACM1"

with open(file_path, "w") as file:
    file.write("t,acc_x,acc_y,acc_z,gyro_x,gyro_y,gyro_z,mag_x,mag_y,mag_z\n")

with serial.Serial(serial_path, 115200) as ser:
    ser.read_all()
    line = ser.readline()
    for i in tqdm(range(N_point)):
        line = ser.readline()
        with open(file_path, "a") as file:
            file.write(str(time.perf_counter()) + ",")
        with open(file_path, "ab") as file:
            file.writelines([line])
