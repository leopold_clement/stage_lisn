import pandas as pd
import seaborn as sb
import matplotlib.pyplot as plt

df = pd.read_csv("tmp/data_conv_statique_ref.csv")
df = df[['gyro_x','gyro_y','gyro_z','acc_x','acc_y','acc_z','mag_x','mag_y','mag_z']]
print("Data valid")
print(df.describe())
sb.pairplot(df)
plt.savefig("data_cov.png")
print(df.cov())