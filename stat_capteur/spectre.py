import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
df = pd.read_csv("data.csv")
fig, axs = plt.subplots(9, 1, sharex=True, figsize=(10, 15))
freq = np.fft.fftshift(np.fft.fftfreq(len(df), 0.1))
for idx, col in enumerate(df.columns):
    serie = df[col]
    arr = serie.to_numpy()
    val = np.average(arr)
    #arr = arr - val
    fft = np.fft.fft(arr)
    #fft[0] = np.nan
    fft = np.fft.fftshift(fft)
    axs[idx].plot(freq, np.abs(fft), label=col)
    axs[idx].set_yscale("log")
    axs[idx].set_ylabel(col)
    axs[idx].grid(which="both")
axs[-1].set_xlabel('Freq (Hz)')
fig.tight_layout()
fig.savefig("specter.png")
