import pandas as pd
import scipy.optimize as optimize
import numpy as np
import matplotlib.pyplot as plt

df = pd.read_csv('tmp/data_raw.csv')
data = df[['mag_x', 'mag_y', 'mag_z']].to_numpy()

mag_reel = 47


def erreur(offset: np.ndarray, norm_type) -> float:
    offsets = np.tile(offset, (data.shape[0], 1))
    corrected = data - offsets
    squared = corrected ** 2
    norm = np.sum(squared, axis=1)
    norm_error = np.abs(norm - mag_reel)
    res = np.linalg.norm(norm_error, norm_type)
    return res


norms = [2, np.inf]

results = [optimize.minimize(erreur, np.array([0, 0, 0]), norm, method='Nelder-Mead')
           for norm in norms]

print(results)

fig, axs = plt.subplots(2, figsize=(15, 12))

ax = axs[0]
ax.plot(np.sqrt(np.sum(data**2, axis=1)), label="Base")

for norm, result in zip(norms, results):
    ax.plot(np.sqrt(np.sum(
        (data - np.tile(result.x, (data.shape[0], 1)))**2, axis=1)),
        label=f"Corrigé avec norm{norm}",
        linestyle="--",
        alpha=0.6)
    print(f'{norm}\t{result.x}\n')
ax.axhline(mag_reel, color="k", label="Réel")
ax.legend()

axs[1].bar([str(norm) for norm in norms], [result.fun for result in results])
axs[1].set_yscale('log')
fig.savefig('tmp/calibration.png', dpi=500)
