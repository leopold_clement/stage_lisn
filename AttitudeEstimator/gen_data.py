import sympy
import sympy.utilities.codegen as codegen
import vestelisn_proto
import numpy as np

kalman = vestelisn_proto.estimation_local.Kalman.NaiveKalmanFilterEstimator(
    np.eye(7), np.eye(7), np.eye(9))

state = sympy.symbols('x:7')

f_sim = kalman.f_sim
h_sim = kalman.h_sim
tmp_F_sim = sympy.derive_by_array(f_sim, state)
tmp_H_sim = sympy.derive_by_array(h_sim, state)

F_sim = sympy.Matrix(tmp_F_sim[:, :, 0])
H_sim = sympy.Matrix(tmp_H_sim[:, :, 0])

sympy.init_printing()
sympy.pprint(f_sim)
sympy.pprint(h_sim)
sympy.pprint(F_sim)
sympy.pprint(H_sim)

[(c_name, c_code), (h_name, c_header)] = codegen.codegen([("symbolic_f", f_sim),
                                                          ('symbolic_h', h_sim),
                                                          ('symbolic_F', F_sim),
                                                          ('symbolic_H', H_sim)], "C99")

with open("src/model_symbolique.c", 'w') as f:
    f.write(c_code)

with open("include/model_symbolique.h", 'w') as f:
    f.write(c_header)
