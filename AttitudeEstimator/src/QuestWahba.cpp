#include "Eigen/Dense"
#include "QuestWahba.hpp"
#include "Physics.hpp"

QuestWahba::QuestWahba()
{
}

QuestWahba::~QuestWahba()
{
}

Eigen::Matrix<float, 4, 4> QuestWahba::get_K(ObservationVector obs)
{
    Eigen::Vector3f acc;
    Eigen::Vector3f mag;
    acc << obs(3), obs(4), obs(5);
    mag << obs(6), obs(7), obs(8);
    Eigen::Matrix<float, 3, 3> B = (1.0f / 2.0f) * ((Physics::ref_acc() * acc.transpose()) + (Physics::ref_mag() * mag.transpose()));
    Eigen::Matrix<float, 3, 3> S = B + B.transpose();
    Eigen::Vector3f z = (1 / 2) * ((Physics::ref_acc().cross(acc)) + (Physics::ref_mag().cross(mag)));
    float sigma = B.trace();
    Eigen::Matrix<float, 4, 4> K;
    K << S, z, z.transpose(), sigma;
    return K;
}

std::string QuestWahba::get_identifier(void)
{
    return std::string("Quest");
}

Eigen::Quaternionf QuestWahba::estimate_q(ObservationVector obs, [[maybe_unused]] Eigen::Quaternionf first_guess)
{
    auto K = this->get_K(obs);
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix4f> es;
    auto r = es.compute(K);
    auto lambdas = r.eigenvalues();
    auto vectors = r.eigenvectors();
    size_t max = 1;
    for (size_t i = 0; i < 4; i++)
    {
        max = (lambdas(i) > lambdas(max)) ? i : max;
    }

    Eigen::Vector4f q_desordre = vectors.col(0);

    Eigen::Quaternionf q = Eigen::Quaternion(q_desordre.w(), q_desordre.x(), q_desordre.y(), q_desordre.z());
    return q;
}
