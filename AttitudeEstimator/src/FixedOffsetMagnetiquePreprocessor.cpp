#include "FixedOffsetMagnetiquePreprocessor.hpp"
#include "Eigen/Dense"
#include "AttitudeTypeDef.hpp"

ObservationVector FixedOffsetMagnetiquePreprocessor::process_stage(ObservationVector obs)
{
    return obs - offset;
}

FixedOffsetMagnetiquePreprocessor::FixedOffsetMagnetiquePreprocessor(Eigen::Matrix<float, 3, 1> mag_offset, ObservationPreprocessor * previous_stage) : ObservationPreprocessor(previous_stage)
{
    offset << 0, 0, 0, 0, 0, 0, mag_offset(0), mag_offset(1), mag_offset(2);
}

FixedOffsetMagnetiquePreprocessor::~FixedOffsetMagnetiquePreprocessor()
{
}
