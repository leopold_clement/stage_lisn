#include <iostream>
#include "AttitudeEstimator.hpp"
#include "Eigen/Dense"
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "rapidcsv.h"
#include <chrono>

#include "argparse/argparse.hpp"

#include "QuestWahba.hpp"
#include "PureWahbaAttitudeEstimator.hpp"
#include "FixedOffsetMagnetiquePreprocessor.hpp"
#include "Physics.hpp"

#include "GeneralKalmanFilter.hpp"
#include "NaiveSystemModel.hpp"
#include "StagedWahbaSystemModel.hpp"
#include "FilterAttitudeEstimator.hpp"
#include "ParticleFilter.hpp"

void load_stream(std::ifstream &infile, std::vector<ObservationVector> &observations)
{
    std::string line;
    std::getline(infile, line);
    std::cout << "line :" << line << std::endl;
    while (!line.empty())
    {
        std::getline(infile, line);
        float cells[10];
        std::stringstream lineStream(line);
        for (int idx = 0; idx < 10; idx++)
        {
            std::string tmp_str;
            std::cerr << idx << " : " << tmp_str << '\n';

            std::getline(lineStream, tmp_str, ',');
            try
            {
                cells[idx] = std::stof(tmp_str);
            }
            catch (const std::exception &e)
            {
                std::cerr << e.what() << '\n';
                return;
            }
        }
        auto obs = ObservationVector(cells + 1);
        observations.push_back(obs);
    }
}

int main([[maybe_unused]] int argc, [[maybe_unused]] char const *argv[])
{
    std::string input_path;
    std::string output_path;

#ifdef DEBUG
    std::cout << "DEBUG MODE" << std::endl;
#endif
#ifdef BENCH
    std::cout << "BENCH MODE" << std::endl;
#endif

#if defined(DEBUG) | defined(BENCH)
    //input_path = "data_conv_statique_ref.csv";
    input_path = "data_conv_deplacement_ref.csv";
    output_path = "data_out/";
#else
    argparse::ArgumentParser cli_tool_parser("Attitude Comparator", "Read from a file, process data, and write to another file");
    cli_tool_parser.add_argument("input_file")
        .help("the input file to read from");
    cli_tool_parser.add_argument("output_file")
        .help("the output file to write to");

    try
    {
        cli_tool_parser.parse_args(argc, argv);
    }
    catch (const std::runtime_error &err)
    {
        std::cerr << err.what() << std::endl;
        std::cerr << cli_tool_parser;
        return 1;
    }

    input_path = cli_tool_parser.get<std::string>("input_file");
    output_path = cli_tool_parser.get<std::string>("output_file");
#endif

    std::ifstream infile(input_path);

    if (!infile.is_open())
    {
        std::cerr << "Error: could not open input file '" << input_path << "'\n";
        return 1;
    }

    rapidcsv::Document data_raw(infile);
    std::vector<ObservationVector> data;
    size_t N_point = data_raw.GetRowCount();
    data.reserve(N_point);

    for (size_t i = 0; i < N_point; i++)
    {
        ObservationVector row(data_raw.GetCell<float>("gyro_x", i),
                              data_raw.GetCell<float>("gyro_y", i),
                              data_raw.GetCell<float>("gyro_z", i),
                              data_raw.GetCell<float>("acc_x", i),
                              data_raw.GetCell<float>("acc_y", i),
                              data_raw.GetCell<float>("acc_z", i),
                              data_raw.GetCell<float>("mag_x", i),
                              data_raw.GetCell<float>("mag_y", i),
                              data_raw.GetCell<float>("mag_z", i));
        data.push_back(row);
    }
    infile.close();

    Eigen::Matrix<float, 3, 1> mag_offset;
    mag_offset << 29.25101319, -11.95425131, 48.00923032;
    mag_offset *= 10 ^ -6;

    FixedOffsetMagnetiquePreprocessor mag_preprocesseur(mag_offset);

    QuestWahba estimateur_position_initial;

    std::vector<AttitudeEstimator *> estimators;
    Eigen::Matrix<float, 7, 1> x_init = estimateur_position_initial.estimate(data[0]);
    auto P_init = 0.1 * Eigen::Matrix<float, 7, 7>::Identity();
    std::cout << x_init << std::endl;

    // ##### VALEURS IMPORTANTES #####//
    Eigen::Matrix<float, 7, 7> Q;

    Q << 0.1 * Eigen::Matrix3f::Ones() , Eigen::Matrix<float, 3, 4>::Zero() , 0.001* Eigen::Matrix<float, 4, 3>::Zero(), Eigen::Matrix4f::Ones();

    Eigen::Matrix<float, 9, 9> R_naive;
    Eigen::Matrix<float, 7, 7> R_wahba; // = (0.1 * Eigen::Matrix<float, 7, 7>::Ones()) + (5 * Eigen::Matrix<float, 7, 7>::Identity());

    R_naive << 6.123665e-06, 1.174314e-07, 2.031692e-07, 1.904171e-06, -1.612770e-06, 5.318652e-07, 4.952062e-11, -7.509114e-11, 4.344858e-11,
        1.174314e-07, 6.396959e-06, -4.099240e-08, -4.603899e-06, 1.943247e-06, -6.455098e-07, 1.987529e-11, -1.067547e-11, 8.059974e-11,
        2.031692e-07, -4.099240e-08, 7.298866e-06, 1.527323e-06, -1.160892e-06, -3.535331e-06, -1.586641e-11, 7.448480e-11, 8.788890e-11,
        1.904171e-06, -4.603899e-06, 1.527323e-06, 8.010352e-04, 2.882042e-05, 1.733448e-05, -1.200682e-09, -2.636229e-09, 1.095273e-09,
        -1.612770e-06, 1.943247e-06, -1.160892e-06, 2.882042e-05, 8.753593e-04, 2.105200e-05, -2.955437e-10, -1.049554e-09, -1.135094e-09,
        5.318652e-07, -6.455098e-07, -3.535331e-06, 1.733448e-05, 2.105200e-05, 8.343414e-04, 5.494200e-10, -6.870277e-10, -4.992104e-10,
        4.952062e-11, 1.987529e-11, -1.586641e-11, -1.200682e-09, -2.955437e-10, 5.494200e-10, 5.344948e-13, 7.962639e-14, -1.011593e-14,
        -7.509114e-11, -1.067547e-11, 7.448480e-11, -2.636229e-09, -1.049554e-09, -6.870277e-10, 7.962639e-14, 6.316566e-13, -4.566549e-14,
        4.344858e-11, 8.059974e-11, 8.788890e-11, 1.095273e-09, -1.135094e-09, -4.992104e-10, -1.011593e-14, -4.566549e-14, 5.142951e-13;

    Eigen::Matrix<float, 3, 3> gyro_noise;

    gyro_noise << 6.123665e-06, 1.174314e-07, 2.031692e-07,
        1.174314e-07, 6.396959e-06, -4.099240e-08,
        2.031692e-07, -4.099240e-08, 7.298866e-06;

    R_wahba << gyro_noise, Eigen::Matrix<float, 3, 4>::Zero(),
        Eigen::Matrix<float, 4, 3>::Zero(), 0.7 * Eigen::Matrix<float, 4, 4>::Identity();

    NaiveSystemModel system_naive(Physics::delta_t, Q, R_naive);

    QuestWahba wahba_q;
    StagedWahbaSystemModel system_wahba(Physics::delta_t, Q, R_wahba, wahba_q);

    GeneralKalmanFilter<7, 9> naiveKalman(x_init, P_init, system_naive);
    FilterAttitudeEstimator<9> naiveKalmanFilterEstimator(&mag_preprocesseur, naiveKalman, system_naive);
    estimators.push_back(&naiveKalmanFilterEstimator);

    GeneralKalmanFilter<7, 7> stagedKalman(x_init, P_init, system_wahba);
    FilterAttitudeEstimator<7> stagedKalmanEstimator(&mag_preprocesseur, stagedKalman, system_wahba);
    estimators.push_back(&stagedKalmanEstimator);

    estimators.push_back(new PureWahbaAttitudeEstimator(new QuestWahba(), &mag_preprocesseur));

    ParticleFilter<7, 9, 10> smallParticle_naive(x_init, system_naive);
    FilterAttitudeEstimator<9> smallParticleEstimator_naive(&mag_preprocesseur, smallParticle_naive, system_naive);
    estimators.push_back(&smallParticleEstimator_naive);

    ParticleFilter<7, 7, 10> smallParticle_staged(x_init, system_wahba);
    FilterAttitudeEstimator<7> smallParticleEstimator_staged(&mag_preprocesseur, smallParticle_staged, system_wahba);
    estimators.push_back(&smallParticleEstimator_staged);

    ParticleFilter<7, 9, 20> mediumParticle(x_init, system_naive);
    FilterAttitudeEstimator<9> mediumParticleEstimator(&mag_preprocesseur, mediumParticle, system_naive);
    estimators.push_back(&mediumParticleEstimator);

    ParticleFilter<7, 9, 50> bigParticle(x_init, system_naive);
    FilterAttitudeEstimator<9> bigParticleEstimator(&mag_preprocesseur, bigParticle, system_naive);
    estimators.push_back(&bigParticleEstimator);

    std::vector<std::vector<StateVector>> results;
    std::vector<AttitudeEstimator *>::iterator it_estimator;

#ifdef DEBUG_1
    for (auto test_est : estimators)
    {
        std::cout << test_est->get_identifieur() << "\n";
    }
#endif

#ifdef BENCH
    const int N_rep = 1000;
    for (int idx_rep = 0; idx_rep < N_rep; idx_rep++)
    {
        for (auto est : estimators)
        {
            std::vector<StateVector> result;
            result.reserve(N_point);
            auto start = std::chrono::high_resolution_clock::now();
            est->batch_process_observation(data, result);
            auto end = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double> elapsed = end - start;
            std::cout << "O0," << est->get_identifieur() << "," << std::to_string(elapsed.count()) << std::endl;
        }
        // std::cout  << idx_rep << "/" << N_rep << std::endl;
    }
#endif

    for (it_estimator = estimators.begin(); it_estimator < estimators.end(); it_estimator++)
    {
        std::vector<StateVector> result;
        result.reserve(N_point);
        it_estimator.base();
        AttitudeEstimator *estimator = *it_estimator;
        estimator->batch_process_observation(data, result);
        results.push_back(result);
    }

    for (size_t idx_estimator = 0; idx_estimator < estimators.size(); idx_estimator++)
    {
        std::ofstream out_file(output_path + estimators[idx_estimator]->get_identifieur() + ".csv");
        out_file << "q_0,q_1,q_2,q_3,q_4\n";
        for (size_t idx_step = 0; idx_step < results[idx_estimator].size(); idx_step++)
        {
            auto q = results[idx_estimator][idx_step];
            out_file << q(3) << "," << q(4) << "," << q(5) << "," << q(6) << "\n";
        }
        out_file.close();
    }
    return 0;
}
