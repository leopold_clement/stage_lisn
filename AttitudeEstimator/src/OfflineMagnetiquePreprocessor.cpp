#include "OfflineMagnetiquePreprocessor.hpp"
#include "Eigen/Dense"
#include "unsupported/Eigen/LevenbergMarquardt"
#include "unsupported/Eigen/NumericalDiff"
#include <cmath>
/*
struct FunctorOfflineMagnetiqueOffsetError : Eigen::DenseFunctor<float, 3, 1>
{
    FunctorOfflineMagnetiqueOffsetError(std::vector<Eigen::Vector3f> data) : datas(data) {}

    std::vector<Eigen::Vector3f> datas;
    int operator()(const InputType &x, ValueType& fvec) const
    {
        auto off = x;

        std::vector<Eigen::Vector3f> unbiaised;
        unbiaised.reserve(datas.size());
        for (auto obs : datas)
            unbiaised.push_back(obs - off);

        std::vector<float> intensity;
        intensity.reserve(datas.size());
        for (auto obs : unbiaised)
            intensity.push_back(obs.norm());

        std::vector<float> error;
        error.reserve(datas.size());
        for (auto obs : intensity)
            error.push_back(fabs(obs));

        double acc = 0;
        for (auto err : error)
            acc += err * err;
        double fitness = sqrt(acc);
        fvec(0, 0) = fitness;

        return 0;
    }
};

OfflineMagnetiquePreprocessor::OfflineMagnetiquePreprocessor(std::vector<ObservationVector> observations)
{
    // Extract magnetique field
    std::vector<Eigen::Vector3f> mag;
    for (auto obs : observations)
    {
        Eigen::Vector3f v;
        v << obs(6), obs(7), obs(8);
        mag.push_back(v);
    }

    // Define the functor
    FunctorOfflineMagnetiqueOffsetError base_functor{mag};
    Eigen::NumericalDiff<FunctorOfflineMagnetiqueOffsetError, Eigen::Central> functor(base_functor);

    // Define the optimization algorithm (Levenberg-Marquardt)
    Eigen::LevenbergMarquardt<Eigen::NumericalDiff<FunctorOfflineMagnetiqueOffsetError, Eigen::Central>> lm(functor);

    // Set the optimization parameters
    // lm.ma = 100;
    // lm.ftol = 1e-8;

    // Perform the optimization
    Eigen::VectorXf x0(3);
    x0 << 0, 0, 0;
    lm.minimize(x0);
    offset << x0(0), x0(1), x0(2);
}
*/