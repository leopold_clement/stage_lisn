#include "Wahba.hpp"

StateVector WahbaEstimator::estimate(const ObservationVector obs, Eigen::Quaternionf first_guess)
{
    auto q_hat = this->estimate_q(obs, first_guess);
    StateVector state;
    state << obs(0), obs(1), obs(2), q_hat.w(), q_hat.x(), q_hat.y(), q_hat.z();
    return state;
}