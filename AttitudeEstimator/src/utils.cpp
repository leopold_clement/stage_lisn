#include "utils.hpp"

StateVector normalisation_quaternion(StateVector state)
{
    Eigen::Quaternionf q(state(3), state(4), state(5), state(6));
    q.normalize();
    StateVector res;
    res << state(0), state(1), state(2), q.w(), q.x(), q.y(), q.z();
    return res;
}