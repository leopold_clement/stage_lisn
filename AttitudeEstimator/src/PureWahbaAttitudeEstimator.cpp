#include "PureWahbaAttitudeEstimator.hpp"
#include "AttitudeEstimator.hpp"

PureWahbaAttitudeEstimator::PureWahbaAttitudeEstimator(WahbaEstimator *estimator,
                                                       ObservationPreprocessor* preprocesseur) : AttitudeEstimator(preprocesseur),
                                                                                                 estimator(estimator)
{
    last_state = StateVector::Zero();
}

std::string PureWahbaAttitudeEstimator::get_identifieur(void) const 
{
    return std::string("Pure ") + estimator->get_identifier();
}

StateVector PureWahbaAttitudeEstimator::get_state(void)const 
{
    return last_state;
}

StateVector PureWahbaAttitudeEstimator::update_state(const ObservationVector new_observation)
{
    last_state = estimator->estimate(new_observation);
    return last_state;
}
