#include "model_symbolique.h"

void wrap_f(double x_init[7], double x_suivant[7], double delta_t, double tau)
{
    symbolic_f(delta_t, tau, x_init[0], x_init[1], x_init[2], x_init[3], x_init[4], x_init[5], x_init[6], x_suivant);
    return;
}

void wrap_h(double x_init[7], double observation[9], double M, double g, double theta)
{
    symbolic_h(M, g, theta, x_init[0], x_init[1], x_init[2], x_init[3], x_init[4], x_init[5], x_init[6], observation);
    return;
}

void wrap_F(double x_init[7], double mat[7][7], double delta_t, double tau)
{
    symbolic_F(delta_t, tau, x_init[0], x_init[1], x_init[2], x_init[3], x_init[4], x_init[5], x_init[6], (double *)mat);
    return;
}

void wrap_H(double x_init[7], double mat[7][9], double M, double g, double theta)
{

    symbolic_H(M, g, theta, x_init[3], x_init[4], x_init[5], x_init[6], (double *)mat);
    return;
}