/******************************************************************************
 *                       Code generated with SymPy 1.12                       *
 *                                                                            *
 *              See http://www.sympy.org/ for more information.               *
 *                                                                            *
 *                       This file is part of 'project'                       *
 ******************************************************************************/
#include "model_symbolique.h"
#include <math.h>

void symbolic_f(double delta_t, double tau, double x0, double x1, double x2, double x3, double x4, double x5, double x6, double *out_4307499007537593705) {

   out_4307499007537593705[0] = -tau*x0 + x0;
   out_4307499007537593705[1] = -tau*x1 + x1;
   out_4307499007537593705[2] = -tau*x2 + x2;
   out_4307499007537593705[3] = delta_t*x3*(-0.5*x0*x4 - 0.5*x1*x5 - 0.5*x2*x6) - delta_t*x4*(0.5*x0*x3 - 0.5*x1*x6 + 0.5*x2*x5) - delta_t*x5*(0.5*x0*x6 + 0.5*x1*x3 - 0.5*x2*x4) - delta_t*x6*(-0.5*x0*x5 + 0.5*x1*x4 + 0.5*x2*x3);
   out_4307499007537593705[4] = delta_t*x3*(0.5*x0*x3 - 0.5*x1*x6 + 0.5*x2*x5) + delta_t*x4*(-0.5*x0*x4 - 0.5*x1*x5 - 0.5*x2*x6) + delta_t*x5*(-0.5*x0*x5 + 0.5*x1*x4 + 0.5*x2*x3) - delta_t*x6*(0.5*x0*x6 + 0.5*x1*x3 - 0.5*x2*x4);
   out_4307499007537593705[5] = delta_t*x3*(0.5*x0*x6 + 0.5*x1*x3 - 0.5*x2*x4) - delta_t*x4*(-0.5*x0*x5 + 0.5*x1*x4 + 0.5*x2*x3) + delta_t*x5*(-0.5*x0*x4 - 0.5*x1*x5 - 0.5*x2*x6) + delta_t*x6*(0.5*x0*x3 - 0.5*x1*x6 + 0.5*x2*x5);
   out_4307499007537593705[6] = delta_t*x3*(-0.5*x0*x5 + 0.5*x1*x4 + 0.5*x2*x3) + delta_t*x4*(0.5*x0*x6 + 0.5*x1*x3 - 0.5*x2*x4) - delta_t*x5*(0.5*x0*x3 - 0.5*x1*x6 + 0.5*x2*x5) + delta_t*x6*(-0.5*x0*x4 - 0.5*x1*x5 - 0.5*x2*x6);

}

void symbolic_h(double M, double g, double theta, double x0, double x1, double x2, double x3, double x4, double x5, double x6, double *out_2785197409655306123) {

   out_2785197409655306123[0] = 2*g*x3*x5 + 2*g*x4*x6;
   out_2785197409655306123[1] = -2*g*x3*x4 + 2*g*x5*x6;
   out_2785197409655306123[2] = g*pow(x3, 2) - g*pow(x4, 2) - g*pow(x5, 2) + g*pow(x6, 2);
   out_2785197409655306123[3] = x0;
   out_2785197409655306123[4] = x1;
   out_2785197409655306123[5] = x2;
   out_2785197409655306123[6] = x3*(M*x3*cos(theta) + M*x5*sin(theta)) - x4*(-M*x4*cos(theta) - M*x6*sin(theta)) + x5*(M*x3*sin(theta) - M*x5*cos(theta)) - x6*(-M*x4*sin(theta) + M*x6*cos(theta));
   out_2785197409655306123[7] = x3*(-M*x4*sin(theta) + M*x6*cos(theta)) - x4*(M*x3*sin(theta) - M*x5*cos(theta)) - x5*(-M*x4*cos(theta) - M*x6*sin(theta)) - x6*(-M*x3*cos(theta) - M*x5*sin(theta));
   out_2785197409655306123[8] = x3*(M*x3*sin(theta) - M*x5*cos(theta)) + x4*(-M*x4*sin(theta) + M*x6*cos(theta)) - x5*(M*x3*cos(theta) + M*x5*sin(theta)) - x6*(-M*x4*cos(theta) - M*x6*sin(theta));

}

void symbolic_F(double delta_t, double tau, double x0, double x1, double x2, double x3, double x4, double x5, double x6, double *out_5904763717903364828) {

   out_5904763717903364828[0] = 1 - tau;
   out_5904763717903364828[1] = 0;
   out_5904763717903364828[2] = 0;
   out_5904763717903364828[3] = -1.0*delta_t*x3*x4;
   out_5904763717903364828[4] = 0.5*delta_t*pow(x3, 2) - 0.5*delta_t*pow(x4, 2) - 0.5*delta_t*pow(x5, 2) - 0.5*delta_t*pow(x6, 2);
   out_5904763717903364828[5] = 1.0*delta_t*x3*x6;
   out_5904763717903364828[6] = -1.0*delta_t*x3*x5;
   out_5904763717903364828[7] = 0;
   out_5904763717903364828[8] = 1 - tau;
   out_5904763717903364828[9] = 0;
   out_5904763717903364828[10] = -1.0*delta_t*x3*x5;
   out_5904763717903364828[11] = -1.0*delta_t*x3*x6;
   out_5904763717903364828[12] = 0.5*delta_t*pow(x3, 2) - 0.5*delta_t*pow(x4, 2) - 0.5*delta_t*pow(x5, 2) - 0.5*delta_t*pow(x6, 2);
   out_5904763717903364828[13] = 1.0*delta_t*x3*x4;
   out_5904763717903364828[14] = 0;
   out_5904763717903364828[15] = 0;
   out_5904763717903364828[16] = 1 - tau;
   out_5904763717903364828[17] = -1.0*delta_t*x3*x6;
   out_5904763717903364828[18] = 1.0*delta_t*x3*x5;
   out_5904763717903364828[19] = -1.0*delta_t*x3*x4;
   out_5904763717903364828[20] = 0.5*delta_t*pow(x3, 2) - 0.5*delta_t*pow(x4, 2) - 0.5*delta_t*pow(x5, 2) - 0.5*delta_t*pow(x6, 2);
   out_5904763717903364828[21] = 0;
   out_5904763717903364828[22] = 0;
   out_5904763717903364828[23] = 0;
   out_5904763717903364828[24] = -0.5*delta_t*x0*x4 - 0.5*delta_t*x1*x5 - 0.5*delta_t*x2*x6 + delta_t*(-0.5*x0*x4 - 0.5*x1*x5 - 0.5*x2*x6);
   out_5904763717903364828[25] = 0.5*delta_t*x0*x3 - 0.5*delta_t*x1*x6 + 0.5*delta_t*x2*x5 + delta_t*(0.5*x0*x3 - 0.5*x1*x6 + 0.5*x2*x5);
   out_5904763717903364828[26] = 0.5*delta_t*x0*x6 + 0.5*delta_t*x1*x3 - 0.5*delta_t*x2*x4 + delta_t*(0.5*x0*x6 + 0.5*x1*x3 - 0.5*x2*x4);
   out_5904763717903364828[27] = -0.5*delta_t*x0*x5 + 0.5*delta_t*x1*x4 + 0.5*delta_t*x2*x3 + delta_t*(-0.5*x0*x5 + 0.5*x1*x4 + 0.5*x2*x3);
   out_5904763717903364828[28] = 0;
   out_5904763717903364828[29] = 0;
   out_5904763717903364828[30] = 0;
   out_5904763717903364828[31] = -0.5*delta_t*x0*x3 - 0.5*delta_t*x1*x6 + 0.5*delta_t*x2*x5 - delta_t*(0.5*x0*x3 - 0.5*x1*x6 + 0.5*x2*x5);
   out_5904763717903364828[32] = -0.5*delta_t*x0*x4 + 0.5*delta_t*x1*x5 + 0.5*delta_t*x2*x6 + delta_t*(-0.5*x0*x4 - 0.5*x1*x5 - 0.5*x2*x6);
   out_5904763717903364828[33] = -0.5*delta_t*x0*x5 - 0.5*delta_t*x1*x4 - 0.5*delta_t*x2*x3 - delta_t*(-0.5*x0*x5 + 0.5*x1*x4 + 0.5*x2*x3);
   out_5904763717903364828[34] = -0.5*delta_t*x0*x6 + 0.5*delta_t*x1*x3 - 0.5*delta_t*x2*x4 + delta_t*(0.5*x0*x6 + 0.5*x1*x3 - 0.5*x2*x4);
   out_5904763717903364828[35] = 0;
   out_5904763717903364828[36] = 0;
   out_5904763717903364828[37] = 0;
   out_5904763717903364828[38] = 0.5*delta_t*x0*x6 - 0.5*delta_t*x1*x3 - 0.5*delta_t*x2*x4 - delta_t*(0.5*x0*x6 + 0.5*x1*x3 - 0.5*x2*x4);
   out_5904763717903364828[39] = -0.5*delta_t*x0*x5 - 0.5*delta_t*x1*x4 + 0.5*delta_t*x2*x3 + delta_t*(-0.5*x0*x5 + 0.5*x1*x4 + 0.5*x2*x3);
   out_5904763717903364828[40] = 0.5*delta_t*x0*x4 - 0.5*delta_t*x1*x5 + 0.5*delta_t*x2*x6 + delta_t*(-0.5*x0*x4 - 0.5*x1*x5 - 0.5*x2*x6);
   out_5904763717903364828[41] = -0.5*delta_t*x0*x3 - 0.5*delta_t*x1*x6 - 0.5*delta_t*x2*x5 - delta_t*(0.5*x0*x3 - 0.5*x1*x6 + 0.5*x2*x5);
   out_5904763717903364828[42] = 0;
   out_5904763717903364828[43] = 0;
   out_5904763717903364828[44] = 0;
   out_5904763717903364828[45] = -0.5*delta_t*x0*x5 + 0.5*delta_t*x1*x4 - 0.5*delta_t*x2*x3 - delta_t*(-0.5*x0*x5 + 0.5*x1*x4 + 0.5*x2*x3);
   out_5904763717903364828[46] = -0.5*delta_t*x0*x6 - 0.5*delta_t*x1*x3 - 0.5*delta_t*x2*x4 - delta_t*(0.5*x0*x6 + 0.5*x1*x3 - 0.5*x2*x4);
   out_5904763717903364828[47] = 0.5*delta_t*x0*x3 - 0.5*delta_t*x1*x6 - 0.5*delta_t*x2*x5 + delta_t*(0.5*x0*x3 - 0.5*x1*x6 + 0.5*x2*x5);
   out_5904763717903364828[48] = 0.5*delta_t*x0*x4 + 0.5*delta_t*x1*x5 - 0.5*delta_t*x2*x6 + delta_t*(-0.5*x0*x4 - 0.5*x1*x5 - 0.5*x2*x6);

}

void symbolic_H(double M, double g, double theta, double x3, double x4, double x5, double x6, double *out_7472469397222049595) {

   out_7472469397222049595[0] = 0;
   out_7472469397222049595[1] = 0;
   out_7472469397222049595[2] = 0;
   out_7472469397222049595[3] = 1;
   out_7472469397222049595[4] = 0;
   out_7472469397222049595[5] = 0;
   out_7472469397222049595[6] = 0;
   out_7472469397222049595[7] = 0;
   out_7472469397222049595[8] = 0;
   out_7472469397222049595[9] = 0;
   out_7472469397222049595[10] = 0;
   out_7472469397222049595[11] = 0;
   out_7472469397222049595[12] = 0;
   out_7472469397222049595[13] = 1;
   out_7472469397222049595[14] = 0;
   out_7472469397222049595[15] = 0;
   out_7472469397222049595[16] = 0;
   out_7472469397222049595[17] = 0;
   out_7472469397222049595[18] = 0;
   out_7472469397222049595[19] = 0;
   out_7472469397222049595[20] = 0;
   out_7472469397222049595[21] = 0;
   out_7472469397222049595[22] = 0;
   out_7472469397222049595[23] = 1;
   out_7472469397222049595[24] = 0;
   out_7472469397222049595[25] = 0;
   out_7472469397222049595[26] = 0;
   out_7472469397222049595[27] = 2*g*x5;
   out_7472469397222049595[28] = -2*g*x4;
   out_7472469397222049595[29] = 2*g*x3;
   out_7472469397222049595[30] = 0;
   out_7472469397222049595[31] = 0;
   out_7472469397222049595[32] = 0;
   out_7472469397222049595[33] = 2*M*x3*cos(theta) + 2*M*x5*sin(theta);
   out_7472469397222049595[34] = -2*M*x4*sin(theta) + 2*M*x6*cos(theta);
   out_7472469397222049595[35] = 2*M*x3*sin(theta) - 2*M*x5*cos(theta);
   out_7472469397222049595[36] = 2*g*x6;
   out_7472469397222049595[37] = -2*g*x3;
   out_7472469397222049595[38] = -2*g*x4;
   out_7472469397222049595[39] = 0;
   out_7472469397222049595[40] = 0;
   out_7472469397222049595[41] = 0;
   out_7472469397222049595[42] = 2*M*x4*cos(theta) + 2*M*x6*sin(theta);
   out_7472469397222049595[43] = -2*M*x3*sin(theta) + 2*M*x5*cos(theta);
   out_7472469397222049595[44] = -2*M*x4*sin(theta) + 2*M*x6*cos(theta);
   out_7472469397222049595[45] = 2*g*x3;
   out_7472469397222049595[46] = 2*g*x6;
   out_7472469397222049595[47] = -2*g*x5;
   out_7472469397222049595[48] = 0;
   out_7472469397222049595[49] = 0;
   out_7472469397222049595[50] = 0;
   out_7472469397222049595[51] = 2*M*x3*sin(theta) - 2*M*x5*cos(theta);
   out_7472469397222049595[52] = 2*M*x4*cos(theta) + 2*M*x6*sin(theta);
   out_7472469397222049595[53] = -2*M*x3*cos(theta) - 2*M*x5*sin(theta);
   out_7472469397222049595[54] = 2*g*x4;
   out_7472469397222049595[55] = 2*g*x5;
   out_7472469397222049595[56] = 2*g*x6;
   out_7472469397222049595[57] = 0;
   out_7472469397222049595[58] = 0;
   out_7472469397222049595[59] = 0;
   out_7472469397222049595[60] = 2*M*x4*sin(theta) - 2*M*x6*cos(theta);
   out_7472469397222049595[61] = 2*M*x3*cos(theta) + 2*M*x5*sin(theta);
   out_7472469397222049595[62] = 2*M*x4*cos(theta) + 2*M*x6*sin(theta);

}
