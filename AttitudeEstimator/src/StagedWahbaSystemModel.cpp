#include "StagedWahbaSystemModel.hpp"
#include "model_wrapper.hpp"
#include <random>
#include "AttitudeEstimator.hpp"
#include <cmath>
#include "Wahba.hpp"
#include "Physics.hpp"

StagedWahbaSystemModel::StagedWahbaSystemModel(float dt, Eigen::Matrix<float, N_state, N_state> Q, Eigen::Matrix<float, 7, 7> R, WahbaEstimator& _wahba) : dt(dt), Q_(Q), R_(R), wahba(_wahba)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    random_gen = gen;
}

StagedWahbaSystemModel::~StagedWahbaSystemModel()
{
}

Eigen::Matrix<float, 7, 1> StagedWahbaSystemModel::conv(const ObservationVector obs)const 
{
    return wahba.estimate(obs);
}

StateVector StagedWahbaSystemModel::f(const StateVector state)const 
{
    double input[7];
    for (size_t i = 0; i < 7; i++)
    {
        input[i] = (double)state(i);
    }
    double output[7];
    wrap_f(input, output, dt, Physics::tau_meca);
    Eigen::Matrix<float, 7, 1> out = Eigen::Matrix<float, 7, 1>::Zero();
    for (size_t i = 0; i < 7; i++)
    {
        out(i) = (float)output[i];
    }
    return out;
}

Eigen::Matrix<float, 7, 1> StagedWahbaSystemModel::h(const StateVector state)const 
{
return state;
}

StateVector StagedWahbaSystemModel::f_noisy(const StateVector state)const 
{
    auto out = f(state);
    Eigen::Matrix<float, N_state, 1> z;
    std::normal_distribution<float> distribution(0, 1);
    for (int idx = 0; idx < N_state; idx++)
    {
        z(idx) = distribution(random_gen);
    }
    Eigen::LLT<Eigen::Matrix<float, N_state, N_state>> lltOfCov(Q(state));
    auto L = lltOfCov.matrixL();
    return out + (L * z);
}

float StagedWahbaSystemModel::prob_obs(const StateVector state,const  Eigen::Matrix<float, 7, 1> obs)const 
{
    Eigen::Matrix<float, 7, 1> x = h(state);
    Eigen::Matrix<float, 7, 1> err = obs - x;
    Eigen::Matrix<float, 1, 1> e = -1 / 2 * Eigen::Transpose<Eigen::Matrix<float, 7, 1>>(err) * R(state) * err;
    float exp = expf(e(0));
    float p = exp / (powf(2 * M_PI, 7 / 2) * powf(R(state).determinant(), 1 / 2));
    return p;
}

Eigen::Matrix<float, N_state, N_state> StagedWahbaSystemModel::F(const StateVector state)const 
{
    double input[7];
    for (size_t i = 0; i < 7; i++)
    {
        input[i] = (double)state(i);
    }
    double output[7][7];
    wrap_F(input, output, dt, Physics::tau_meca);
    Eigen::Matrix<float, 7, 7> out = Eigen::Matrix<float, 7, 7>::Zero();
    for (size_t i = 0; i < 7; i++)
        for (size_t j = 0; j < 7; j++)
        {
            out(i, j) = output[i][j];
        }
    return out;
}

Eigen::Matrix<float, 7, N_state> StagedWahbaSystemModel::H([[maybe_unused]]const  StateVector state)const 
{
    return Eigen::Matrix<float, 7, 7>::Identity();
}

Eigen::Matrix<float, N_state, N_state> StagedWahbaSystemModel::Q([[maybe_unused]]const StateVector state)const 
{
    return Q_;
}

Eigen::Matrix<float, 7, 7> StagedWahbaSystemModel::R([[maybe_unused]]const StateVector state)const 
{
    return R_;
}

std::string StagedWahbaSystemModel::name(void)const 
{
return std::string("model staged with ") + wahba.get_identifier();
}
