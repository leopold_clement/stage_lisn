#include "ObservationPreprocessor.hpp"

ObservationPreprocessor::ObservationPreprocessor(ObservationPreprocessor *previous_stage) : previous_stage(previous_stage)
{
}

ObservationVector ObservationPreprocessor::process(ObservationVector obs)
{
    if (previous_stage == nullptr)
        return process_stage(obs);
    return process_stage(previous_stage->process(obs));
}

ObservationPreprocessor::~ObservationPreprocessor()
{
}
