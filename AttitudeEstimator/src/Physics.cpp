#include "Physics.hpp"
#include "Eigen/Dense"
#include <cmath>

using namespace Physics;

const Eigen::Vector3f Physics::ref_acc(void){
    Eigen::Vector3f vec;
    vec << 0, 0, g_paris;
    return vec;
}
const Eigen::Vector3f Physics::ref_mag(void){
    Eigen::Vector3f vec;
    vec << cos(theta_paris), 0, sin(theta_paris);
    vec *= m_paris;
    return vec;
}