#include "AttitudeEstimator.hpp"

StateVector AttitudeEstimator::process_observation(const ObservationVector new_observation)
{
    auto obs = (preprocesseur != nullptr) ? preprocesseur->process(new_observation) : new_observation;
    return update_state(obs);
}

void AttitudeEstimator::batch_process_observation(std::vector<ObservationVector> &observations, std::vector<StateVector> &states)
{
    for (std::vector<ObservationVector>::iterator it = observations.begin(); it != observations.end(); ++it)
    {
        auto obs = *it;
        auto state = process_observation(obs);
        states.push_back(state);
    }
}

AttitudeEstimator::AttitudeEstimator(ObservationPreprocessor *_preprocesseur) : preprocesseur(_preprocesseur)
{
}

