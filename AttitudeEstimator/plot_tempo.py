import pandas
import seaborn
import matplotlib.pyplot as plt

df = pandas.read_csv('tempo_comp_optim_x86.csv')

df['Cycles'] = 3e9 * 1 / 1000 * df['durée']

print(df.describe())

fig, ax = plt.subplots()
plt.rcParams["figure.figsize"] = [9.50, 5]
plt.rcParams["figure.autolayout"] = True

p = seaborn.barplot(df, x="Cycles", y="Estimateur", hue="Optimisation", errorbar="sd")
df = df.groupby(['Estimateur', 'Optimisation']).mean()
# plt.xticks(rotation=90)
plt.xscale('log')
plt.grid(True, 'both', "x")
plt.tight_layout()
plt.savefig("tempo.png")

print(df)
