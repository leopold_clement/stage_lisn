#pragma once
#include "AttitudeEstimator.hpp"
#include "Eigen/Dense"
#include "string"
#include "utils.hpp"

template <int N_obs_interne>
class SystemModel
{
private:
public:
    /// @brief 
    /// @param state 
    /// @return 
    virtual StateVector f(const StateVector state) const = 0;
    /// @brief 
    /// @param state 
    /// @return 
    virtual Eigen::Matrix<float, N_obs_interne, 1> h(const StateVector state) const = 0;

    /// @brief 
    /// @param obs 
    /// @return 
    virtual Eigen::Matrix<float, N_obs_interne, 1> conv(const ObservationVector obs) const = 0;

    /// @brief 
    /// @param state 
    /// @return 
    virtual StateVector f_noisy(const StateVector state) const = 0;
    /// @brief 
    /// @param state 
    /// @param obs 
    /// @return 
    virtual float prob_obs(const StateVector state, const Eigen::Matrix<float, N_obs_interne, 1> obs) const = 0;

    /// @brief 
    /// @param state 
    /// @return 
    virtual Eigen::Matrix<float, N_state, N_state> F(const StateVector state) const = 0;
    /// @brief 
    /// @param state 
    /// @return 
    virtual Eigen::Matrix<float, N_obs_interne, N_state> H(const StateVector state) const = 0;
    /// @brief 
    /// @param state 
    /// @return 
    virtual Eigen::Matrix<float, N_state, N_state> Q(const StateVector state) const = 0;
    /// @brief 
    /// @param state 
    /// @return 
    virtual Eigen::Matrix<float, N_obs_interne, N_obs_interne> R(const StateVector state) const = 0;
    /// @brief Apply constains to a statevector
    /// @param state
    /// @return 
    virtual StateVector constrain(const StateVector state) const;
    virtual std::string name(void) const = 0;
    virtual ~SystemModel() {}
};

template <int N_obs_interne>
inline StateVector SystemModel<N_obs_interne>::constrain(const StateVector state) const
{
    return normalisation_quaternion(state);
}
