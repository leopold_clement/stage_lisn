#pragma once

#include "AttitudeEstimator.hpp"
#include "Wahba.hpp"

class PureWahbaAttitudeEstimator : public AttitudeEstimator
{
private:
    WahbaEstimator* estimator;
    StateVector last_state;
public:
    PureWahbaAttitudeEstimator(WahbaEstimator* estimator, ObservationPreprocessor *preprocesseur);
    ~PureWahbaAttitudeEstimator();

    std::string get_identifieur(void) const override;
    StateVector get_state(void)const  override;
    StateVector update_state(const ObservationVector  new_observation) override;
};