#pragma once
#include "Eigen/Dense"
#include "AttitudeTypeDef.hpp"

/// @brief Interface for a preprocessor
class ObservationPreprocessor
{
private:
    ObservationPreprocessor* previous_stage;
    virtual ObservationVector process_stage(ObservationVector obs) = 0;
public:
    /// @brief Create a preprocessor
    /// @param previous_stage pointer to the previous stage
    ObservationPreprocessor(ObservationPreprocessor* previous_stage = nullptr);
    /// @brief Process an observation
    /// @param obs observation to process
    /// @return Processed observation
    ObservationVector process(ObservationVector obs);
    virtual ~ObservationPreprocessor() = 0;
};
