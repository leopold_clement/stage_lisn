
#ifndef __Physics_Estimation_H
#define __Physics_Estimation_H

#include "Eigen/Dense"

/// @brief Namespace for physical constante
/// @todo Make it compatible with other location
namespace Physics
{
    /// @brief Sampling period of the device
    const float delta_t = 0.050;
    /// @brief Mecanical decay time
    const float tau_meca = 0.1;

    /// @brief gravitational acceleration, m/s
    const float g_paris = 9.81;
    /// @brief magnetique intensity, T
    const float m_paris = 0.5;
    /// @brief magnetique angle, rad
    const float theta_paris = 60 * 3.14 / 360;

    /// @brief reference magnetique vector
    const Eigen::Vector3f ref_mag(void);
    /// @brief reference acceleration vector
    const Eigen::Vector3f ref_acc(void);
} // namespace Physics

#endif