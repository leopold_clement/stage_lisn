#pragma once

#include "Eigen/Dense"
#include "string"

/// @brief Interface for state estimation filter
/// @tparam N_etat Number of state of the system
/// @tparam N_observation Number of observation of the system
template <int N_etat, int N_observation>
class StateEstimationFilter
{
private:
    /* data */
public:
    /// @brief Predict the next state
    /// @return the predicted next state
    virtual Eigen::Matrix<float, N_etat, 1> Predict() const  = 0;

    /// @brief Update the state with a processed observation
    /// @param observation processed observation
    /// @return The new estimated state
    virtual Eigen::Matrix<float, N_etat, 1> Update(const Eigen::Matrix<float, N_observation, 1> observation) = 0;

    /// @brief Get the current estimation of the state
    /// @return The current estimated state
    virtual Eigen::Matrix<float, N_etat, 1> Get_state() const  = 0;

    /// @brief Set the current estimation of the state
    /// @return The new estimated state
    virtual void Set_state(const Eigen::Matrix<float, N_etat, 1>) = 0;
    
    virtual ~StateEstimationFilter(){}
    virtual std::string name() const = 0;
};

