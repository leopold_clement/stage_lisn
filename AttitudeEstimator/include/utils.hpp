#pragma once

#include "AttitudeEstimator.hpp"
#include "Eigen/Dense"
#include "Eigen/Eigenvalues"

template <int N_particule>
Eigen::Quaternionf moyenne_quaternion(const Eigen::Quaternionf vecteur[N_particule])
{
    Eigen::Matrix<float, 4, N_particule> Q;
    for (int j = 0; j < N_particule; j++)
    {
        Q(0, j) = vecteur[j].w();
        Q(1, j) = vecteur[j].x();
        Q(2, j) = vecteur[j].y();
        Q(3, j) = vecteur[j].z();
    }
    Eigen::Matrix<float, 4, 4> U = Q * Eigen::Transpose(Q);
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix4f> es;
    auto r = es.compute(U);
    auto lambdas = r.eigenvalues();
    auto vectors = r.eigenvectors();
    size_t max = 1;
    for (size_t i = 0; i < 4; i++)
    {
        max = (lambdas(i) > lambdas(max)) ? i : max;
    }

    Eigen::Vector4f q_desordre = vectors.col(0);

    return Eigen::Quaternionf(q_desordre.w(), q_desordre.x(), q_desordre.y(), q_desordre.z());
}

template <int N_particule>
StateVector moyenne_state(const StateVector vecteurs[N_particule])
{
    Eigen::Quaternionf points[N_particule];
    for (int i = 0; i < N_particule; i++)
    {
        points[i] = Eigen::Quaternionf(vecteurs[i](3), vecteurs[i](4), vecteurs[i](5), vecteurs[i](6));
    }

    auto avg_q = moyenne_quaternion<N_particule>(points);

    Eigen::Matrix<float, 3, 1> avg_rot = Eigen::Matrix<float, 3, 1>::Zero();

    for (int i = 0; i < N_particule; i++)
    {
        Eigen::Matrix<float, 3, 1> rot(vecteurs[i](0), vecteurs[i](1), vecteurs[i](2));
        avg_rot += rot;
    }

    StateVector avg;
    avg << avg_rot(0), avg_rot(1), avg_rot(2), avg_q.w(), avg_q.x(), avg_q.y(), avg_q.z();
    return avg;
}


/// @brief 
/// @param state
/// @return
StateVector normalisation_quaternion(const StateVector state);
