#pragma once

#include "StateEstimationFilter.hpp"
#include <random>
#include "SystemModel.hpp"
#include "string"
#include "Eigen/Eigenvalues"
#include "utils.hpp"

template <int N_etat, int N_observation, int N_particles>
class ParticleFilter : public StateEstimationFilter<N_etat, N_observation>
{
private:
    Eigen::Matrix<float, N_etat, 1> particles[N_particles];
    SystemModel<N_observation> &system;
    std::mt19937 random_gen;

public:
    ParticleFilter(Eigen::Matrix<float, N_etat, 1> initial_state, SystemModel<N_observation> &system);
    ~ParticleFilter();
    Eigen::Matrix<float, N_etat, 1> Update(const Eigen::Matrix<float, N_observation, 1> observation) override;
    Eigen::Matrix<float, N_etat, 1> Predict() const override;
    Eigen::Matrix<float, N_etat, 1> Get_state() const override;
    void Set_state(const Eigen::Matrix<float, N_etat, 1>) override;
    std::string name() const override;
};

template <int N_etat, int N_observation, int N_particles>
ParticleFilter<N_etat, N_observation, N_particles>::ParticleFilter(Eigen::Matrix<float, N_etat, 1> initial_state, SystemModel<N_observation> &system) : system(system)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    random_gen = gen;

    for (int idx = 0; idx < N_particles; idx++)
    {
        particles[idx] = initial_state;
    }
}
template <int N_etat, int N_observation, int N_particles>
ParticleFilter<N_etat, N_observation, N_particles>::~ParticleFilter()
{
}

template <int N_etat, int N_observation, int N_particles>
Eigen::Matrix<float, N_etat, 1> ParticleFilter<N_etat, N_observation, N_particles>::Update(const Eigen::Matrix<float, N_observation, 1> observation)
{
    // calcul des jets de particules
    Eigen::Matrix<float, N_etat, 1> next_state[N_particles];
    for (int idx = 0; idx < N_particles; idx++)
        next_state[idx] = system.constrain(system.f_noisy(particles[idx]));

    // vraissemblance des paticules
    float vraissemblance[N_particles];
    for (int idx = 0; idx < N_particles; idx++)
        vraissemblance[idx] = system.prob_obs(next_state[idx], observation);
    float vraissemblance_total = 0.0;
    for (int idx = 0; idx < N_particles; idx++)
        vraissemblance_total += vraissemblance[idx];
    float vraissemblance_relative[N_particles];
    for (int idx = 0; idx < N_particles; idx++)
        vraissemblance_relative[idx] = vraissemblance[idx] / vraissemblance_total;

    // Estimation de l'état
    Eigen::Matrix<float, N_etat, 1> est = Eigen::Matrix<float, N_etat, 1>::Zero();
    for (int idx = 0; idx < N_particles; idx++)
        est += vraissemblance_relative[idx] * next_state[idx];

    // Resampling
    float vraissemblance_cumulee[N_particles];
    vraissemblance_cumulee[0] = vraissemblance_relative[0];
    for (int idx = 1; idx < N_particles; idx++)
        vraissemblance_cumulee[idx] = vraissemblance_relative[idx] + vraissemblance_cumulee[idx - 1];

    std::uniform_real_distribution<> dist(0, 1);
    for (int idx_particle = 1; idx_particle < N_particles; idx_particle++)
    {
        float tirage = dist(random_gen);
        int idx_ballayage;
        for (idx_ballayage = 0; idx_ballayage < N_particles; idx_ballayage++)
        {
            if (vraissemblance_cumulee[idx_ballayage] >= tirage)
                break;
        }
        particles[idx_particle] = next_state[idx_ballayage];
    }
    return Get_state();
}

template <int N_etat, int N_observation, int N_particles>
Eigen::Matrix<float, N_etat, 1> ParticleFilter<N_etat, N_observation, N_particles>::Predict() const // TODO
{
    return Eigen::Matrix<float, N_etat, 1>();
}

template <int N_etat, int N_observation, int N_particles>
Eigen::Matrix<float, N_etat, 1> ParticleFilter<N_etat, N_observation, N_particles>::Get_state() const
{
    return moyenne_state<N_particles>(particles);
}

template <int N_etat, int N_observation, int N_particles>
inline void ParticleFilter<N_etat, N_observation, N_particles>::Set_state(const Eigen::Matrix<float, N_etat, 1> state)
{
    for (int idx = 0; idx < N_particles; idx++)
        particles[idx] = state;
}

template <int N_etat, int N_observation, int N_particles>
inline std::string ParticleFilter<N_etat, N_observation, N_particles>::name() const
{
    return std::string("Filtre particulaire de taille ") + std::to_string(N_particles);
}
