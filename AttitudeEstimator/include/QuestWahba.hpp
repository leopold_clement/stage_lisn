#pragma once

#include "AttitudeEstimator.hpp"
#include "Wahba.hpp"
#include <string>

class QuestWahba : public WahbaEstimator
{
private:
    /* data */
public:
    QuestWahba(/* args */);
    ~QuestWahba();
    Eigen::Quaternionf estimate_q(ObservationVector obs, Eigen::Quaternionf first_guess = Eigen::Quaternionf(1, 0, 0, 0)) override;
    Eigen::Matrix<float, 4, 4> get_K(ObservationVector obs);
    std::string get_identifier(void) override;
};
