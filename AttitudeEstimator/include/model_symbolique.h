/******************************************************************************
 *                       Code generated with SymPy 1.12                       *
 *                                                                            *
 *              See http://www.sympy.org/ for more information.               *
 *                                                                            *
 *                       This file is part of 'project'                       *
 ******************************************************************************/


#ifndef PROJECT__SYMBOLIC_F__H
#define PROJECT__SYMBOLIC_F__H

void symbolic_f(double delta_t, double tau, double x0, double x1, double x2, double x3, double x4, double x5, double x6, double *out_4307499007537593705);
void symbolic_h(double M, double g, double theta, double x0, double x1, double x2, double x3, double x4, double x5, double x6, double *out_2785197409655306123);
void symbolic_F(double delta_t, double tau, double x0, double x1, double x2, double x3, double x4, double x5, double x6, double *out_5904763717903364828);
void symbolic_H(double M, double g, double theta, double x3, double x4, double x5, double x6, double *out_7472469397222049595);

#endif

