#pragma once
#include "Eigen/Dense"
#include "ObservationPreprocessor.hpp"
#include "AttitudeTypeDef.hpp"
class FixedOffsetMagnetiquePreprocessor : public ObservationPreprocessor
{
private:
    ObservationVector offset;
    ObservationVector process_stage(ObservationVector obs) override;
public:
    FixedOffsetMagnetiquePreprocessor(Eigen::Matrix<float, 3, 1> mag_offset, ObservationPreprocessor *previous_stage = nullptr);
    ~FixedOffsetMagnetiquePreprocessor() override;
};
