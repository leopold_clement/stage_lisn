#pragma once

#include <string>
#include "Eigen/Dense"
#include "ObservationPreprocessor.hpp"

const int N_state = 7;
const int N_mesure_capteur = 9;

/// @brief Interface principale représentant un algoritme d'estimation de l'attitude d'un membre
class AttitudeEstimator
{
private:
    /// @brief Processing of preprocessed Observation
    /// @param new_observation The processed observation
    /// @return The new estimation of the state
    virtual StateVector update_state(const ObservationVector new_observation) = 0;

    /// @brief Pointer to the firth preprocessor
    ObservationPreprocessor *preprocesseur = nullptr;

public:
    /// @brief
    /// @param _preprocesseur
    AttitudeEstimator(ObservationPreprocessor *_preprocesseur);

    /// @brief
    /// @param
    /// @return
    virtual std::string get_identifieur(void) const = 0;

    /// @brief
    /// @param
    /// @return
    virtual StateVector get_state(void) const = 0;

    /// @brief Processe a raw observation
    /// @param new_observation
    /// @return the new estimation of the state
    StateVector process_observation(const ObservationVector new_observation);

    /// @brief Batch process raw observation
    /// @param observations input vector of raw observation
    /// @param states output vector of esimation of state
    void batch_process_observation(std::vector<ObservationVector> &observations, std::vector<StateVector> &states);
};
