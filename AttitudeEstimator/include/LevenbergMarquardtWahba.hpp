#pragma once

#include "Wahba.hpp"
#include "AttitudeEstimator.hpp"
class LevenbergMarquardtWahba : public WahbaEstimator
{
private:
    /* data */
public:
    LevenbergMarquardtWahba(/* args */);
    ~LevenbergMarquardtWahba();
    StateVector estimate(ObservationVector obs) override;
};

LevenbergMarquardtWahba::LevenbergMarquardtWahba(/* args */)
{
}

LevenbergMarquardtWahba::~LevenbergMarquardtWahba()
{
}
