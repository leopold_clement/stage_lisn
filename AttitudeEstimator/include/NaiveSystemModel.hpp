#pragma once

#include "SystemModel.hpp"
#include "Eigen/Dense"
#include <random>

class NaiveSystemModel : public SystemModel<9>
{
private:
    float dt;
    Eigen::Matrix<float, N_state, N_state> Q_;
    Eigen::Matrix<float, 9, 9> R_;
    mutable std::mt19937 random_gen;

public:
    NaiveSystemModel(float dt,
                     Eigen::Matrix<float, N_state, N_state> Q,
                     Eigen::Matrix<float, 9, 9> R);
    ~NaiveSystemModel() override;

    Eigen::Matrix<float, 9, 1> conv(const ObservationVector obs) const override;

    StateVector f(StateVector state) const override;
    Eigen::Matrix<float, 9, 1> h(const StateVector state) const override;

    StateVector f_noisy(const StateVector state) const override;
    float prob_obs(const StateVector state, Eigen::Matrix<float, 9, 1> obs) const override;

    Eigen::Matrix<float, N_state, N_state> F(const StateVector state) const override;
    Eigen::Matrix<float, 9, N_state> H(const StateVector state) const override;

    Eigen::Matrix<float, N_state, N_state> Q(const StateVector state) const override;
    Eigen::Matrix<float, 9, 9> R(const StateVector state) const override;

    std::string name(void) const override;
};
