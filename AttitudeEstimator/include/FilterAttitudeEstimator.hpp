#pragma once

#include "AttitudeEstimator.hpp"
#include "StateEstimationFilter.hpp"
#include "SystemModel.hpp"

template <int N_obs>
class FilterAttitudeEstimator : public AttitudeEstimator
{
private:
    StateEstimationFilter<7, N_obs> &filter;
    SystemModel<N_obs> &systeme;

public:
    FilterAttitudeEstimator(ObservationPreprocessor *preprocesseur,
                            StateEstimationFilter<7, N_obs> &_filter,
                            SystemModel<N_obs> &_systeme) : AttitudeEstimator(preprocesseur), 
                                                            filter(_filter),
                                                            systeme(_systeme) {}
    StateVector update_state(const ObservationVector new_observation)  override
    {
        auto converted_obs = systeme.conv(new_observation);
        auto est = filter.Update(converted_obs);
        return est;
    }
    StateVector get_state(void) const override {
        return filter.Get_state();
    }
    std::string get_identifieur(void) const override{
        return std::string("Estimateur à filtre ") + filter.name() + std::string(" on ") + systeme.name() ;
    }
};
