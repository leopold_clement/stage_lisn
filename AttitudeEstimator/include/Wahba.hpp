#include "AttitudeEstimator.hpp"
#include "Eigen/Dense"
#include <string>

#pragma once

/// @brief Interface pour les estimateurs de Wahba
class WahbaEstimator
{
private:
    /* data */
public:
    /// @brief Estimate the full state vector from an observation
    /// @param obs processed observation
    /// @param first_guess stat point for optimisation
    /// @return the estimated state vector
    StateVector estimate(const ObservationVector obs, Eigen::Quaternionf first_guess = Eigen::Quaternionf(1, 0, 0, 0));
    
    /// @brief Estimate the rotation quaternion from an observation
    /// @param obs processed observation
    /// @param first_guess stat point for optimisation
    /// @return the estimated rotation quaternion
    virtual Eigen::Quaternionf estimate_q(const ObservationVector obs, const Eigen::Quaternionf first_guess = Eigen::Quaternionf(1, 0, 0, 0)) = 0;

    virtual std::string get_identifier(void) = 0;
};
