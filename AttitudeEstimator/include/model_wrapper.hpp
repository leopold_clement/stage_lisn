#pragma once
#include "model_symbolique.h"

void wrap_f(double x_init[7], double x_suivant[7], double delta_t, double tau);


void wrap_h(double x_init[7], double observation[9], double M, double g, double theta);


void wrap_F(double x_init[7], double mat[7][7], double delta_t, double tau);


void wrap_H(double x_init[7], double mat[7][9], double M, double g, double theta);
