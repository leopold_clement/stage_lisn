#pragma once
#include "Eigen/Dense"
#include "ObservationPreprocessor.hpp"
#include "AttitudeTypeDef.hpp"
#if false
class OfflineMagnetiquePreprocessor : public  ObservationPreprocessor
{
private:
    Eigen::Vector3f offset;
    ObservationVector process_stage(ObservationVector obs) override;
public:
    OfflineMagnetiquePreprocessor(std::vector<ObservationVector> observations);
    ~OfflineMagnetiquePreprocessor();
};
#endif