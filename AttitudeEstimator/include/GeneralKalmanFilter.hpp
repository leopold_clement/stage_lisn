#pragma once

#include "StateEstimationFilter.hpp"
#include "SystemModel.hpp"

template <int N_etat, int N_observation>
class GeneralKalmanFilter : public StateEstimationFilter<N_etat, N_observation>
{
private:
    Eigen::Matrix<float, N_etat, 1> x;
    Eigen::Matrix<float, N_etat, N_etat> P;

    SystemModel<N_observation> &system;

    Eigen::Matrix<float, N_etat, 1> f_self() const 
    {
        return system.f(x);
    }
    Eigen::Matrix<float, N_observation, 1> h_self() const 
    {
        return system.h(x);
    }

public:
    GeneralKalmanFilter(Eigen::Matrix<float, N_etat, 1> X_init,
                        Eigen::Matrix<float, N_etat, N_etat> P_init,
                        SystemModel<N_observation> &system) : x(X_init),
                                                              P(P_init),
                                                              system(system)
    {
    }
    ~GeneralKalmanFilter() {}
    Eigen::Matrix<float, N_etat, 1> Predict() const override
    {
        return f_self();
    }
    Eigen::Matrix<float, N_etat, 1> Update(const Eigen::Matrix<float, N_observation, 1> observation) override
    {
        Eigen::Matrix<float, N_etat, 1> hat_x = Predict();
        Eigen::Matrix<float, N_etat, N_etat> hat_P = system.F(x) * P * system.F(x).transpose() + system.Q(x);

        Eigen::Matrix<float, N_observation, 1> y = observation - system.h(x);
        Eigen::Matrix<float, N_observation, N_observation> S = (system.H(x) * hat_P * system.H(x).transpose()) + system.R(x);
        Eigen::Matrix<float, N_etat, N_observation> K = hat_P * system.H(x).transpose() * S.inverse();
        x = system.constrain(hat_x + K * y);
        P = (Eigen::Matrix<float, 7, 7>::Identity() - K * system.H(x)) * hat_P;
        return x;
    }
    Eigen::Matrix<float, N_etat, 1> Get_state() const override
    {
        return x;
    }
    void Set_state(const Eigen::Matrix<float, N_etat, 1> new_value) override
    {
        x = new_value;
        return;
    }

    std::string name(void) const override
    {
        return std::string("Kalman");
    }
};
