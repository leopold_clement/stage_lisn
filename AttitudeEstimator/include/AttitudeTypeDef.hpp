#pragma once
#include "Eigen/Dense"

/// @brief StateVector, avec (w,q)
typedef Eigen::Matrix<float, 7, 1> StateVector;

/// @brief ObservationVector (omega, acc, mag)
typedef Eigen::Matrix<float, 9, 1> ObservationVector;