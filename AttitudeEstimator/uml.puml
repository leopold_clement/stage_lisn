@startuml





/' Objects '/

abstract class AttitudeEstimator {
	+AttitudeEstimator(ObservationPreprocessor* _preprocesseur)
	-preprocesseur : ObservationPreprocessor*
	+{abstract} get_state() : StateVector {query}
	+process_observation(const ObservationVector new_observation) : StateVector
	-{abstract} update_state(const ObservationVector new_observation) : StateVector
	+{abstract} get_identifieur() : std::string {query}
	+batch_process_observation(std::vector<ObservationVector>& observations, std::vector<StateVector>& states) : void
}


class ComplementayObserver {
	+ComplementayObserver()
	+~ComplementayObserver()
	-wahba : WahbaEstimator*
}


class FilterAttitudeEstimator <template<int N_obs>> {
	+FilterAttitudeEstimator(ObservationPreprocessor* preprocesseur, StateEstimationFilter<7, N_obs>& _filter, SystemModel<N_obs>& _systeme)
	-filter : StateEstimationFilter<7, N_obs>&
	+get_state() : StateVector {query}
	+update_state(const ObservationVector new_observation) : StateVector
	-systeme : SystemModel<N_obs>&
	+get_identifieur() : std::string {query}
}


class FixedOffsetMagnetiquePreprocessor {
	+FixedOffsetMagnetiquePreprocessor(Eigen::Matrix<float, 3, 1> mag_offset, ObservationPreprocessor* previous_stage)
	+~FixedOffsetMagnetiquePreprocessor()
	-offset : ObservationVector
	-process_stage(ObservationVector obs) : ObservationVector
}


class GeneralKalmanFilter <template<int N_etat, int N_observation>> {
	+GeneralKalmanFilter(Eigen::Matrix<float, N_etat, 1> X_init, Eigen::Matrix<float, N_etat, N_etat> P_init, SystemModel<N_observation>& system)
	+~GeneralKalmanFilter()
	+Get_state() : Eigen::Matrix<float , N_etat , 1> {query}
	+Predict() : Eigen::Matrix<float , N_etat , 1> {query}
	+Update(const Eigen::Matrix<float, N_observation, 1> observation) : Eigen::Matrix<float , N_etat , 1>
	-f_self() : Eigen::Matrix<float , N_etat , 1> {query}
	-h_self() : Eigen::Matrix<float , N_observation , 1> {query}
	-x : Eigen::Matrix<float, N_etat, 1>
	-P : Eigen::Matrix<float, N_etat, N_etat>
	-system : SystemModel<N_observation>&
	+name() : std::string {query}
	+Set_state(const Eigen::Matrix<float, N_etat, 1> new_value) : void
}


class LevenbergMarquardtWahba {
	+LevenbergMarquardtWahba()
	+~LevenbergMarquardtWahba()
	+estimate(ObservationVector obs) : StateVector
}


class NaiveSystemModel {
	+NaiveSystemModel(float dt, Eigen::Matrix<float, N_state, N_state> Q, Eigen::Matrix<float, 9, 9> R)
	+~NaiveSystemModel()
	+conv(const ObservationVector obs) : Eigen::Matrix<float , 9 , 1> {query}
	+h(const StateVector state) : Eigen::Matrix<float , 9 , 1> {query}
	+R(const StateVector state) : Eigen::Matrix<float , 9 , 9> {query}
	+H(const StateVector state) : Eigen::Matrix<float , 9 , N_state> {query}
	+F(const StateVector state) : Eigen::Matrix<float , N_state , N_state> {query}
	+Q(const StateVector state) : Eigen::Matrix<float , N_state , N_state> {query}
	-R_ : Eigen::Matrix<float, 9, 9>
	-Q_ : Eigen::Matrix<float, N_state, N_state>
	+f(StateVector state) : StateVector {query}
	+f_noisy(const StateVector state) : StateVector {query}
	-dt : float
	+prob_obs(const StateVector state, Eigen::Matrix<float, 9, 1> obs) : float {query}
	-random_gen : mutable std::mt19937
	+name() : std::string {query}
}


abstract class ObservationPreprocessor {
	+ObservationPreprocessor(ObservationPreprocessor* previous_stage)
	+{abstract} ~ObservationPreprocessor()
	-previous_stage : ObservationPreprocessor*
	+process(ObservationVector obs) : ObservationVector
	-{abstract} process_stage(ObservationVector obs) : ObservationVector
}


class OfflineMagnetiquePreprocessor {
	+OfflineMagnetiquePreprocessor(std::vector<ObservationVector> observations)
	+~OfflineMagnetiquePreprocessor()
	-offset : Eigen::Vector3f
	-process_stage(ObservationVector obs) : ObservationVector
}


class ParticleFilter <template<int N_etat, int N_observation, int N_particles>> {
	+ParticleFilter(Eigen::Matrix<float, N_etat, 1> initial_state, SystemModel<N_observation>& system)
	+~ParticleFilter()
	+Get_state() : Eigen::Matrix<float , N_etat , 1> {query}
	+Predict() : Eigen::Matrix<float , N_etat , 1> {query}
	+Update(const Eigen::Matrix<float, N_observation, 1> observation) : Eigen::Matrix<float , N_etat , 1>
	-particles : Eigen::Matrix<float, N_etat, 1>
	-system : SystemModel<N_observation>&
	-random_gen : std::mt19937
	+name() : std::string {query}
	+Set_state(const Eigen::Matrix<float, N_etat, 1>) : void
}


class PureWahbaAttitudeEstimator {
	+PureWahbaAttitudeEstimator(WahbaEstimator* estimator, ObservationPreprocessor* preprocesseur)
	+~PureWahbaAttitudeEstimator()
	+get_state() : StateVector {query}
	-last_state : StateVector
	+update_state(const ObservationVector new_observation) : StateVector
	-estimator : WahbaEstimator*
	+get_identifieur() : std::string {query}
}


class QuestWahba {
	+QuestWahba()
	+~QuestWahba()
	+get_K(ObservationVector obs) : Eigen::Matrix<float , 4 , 4>
	+estimate_q(ObservationVector obs, Eigen::Quaternionf first_guess) : Eigen::Quaternionf
	+get_identifier() : std::string
}


class StagedWahbaSystemModel {
	+StagedWahbaSystemModel(float dt, Eigen::Matrix<float, N_state, N_state> Q, Eigen::Matrix<float, 7, 7> R, WahbaEstimator& _wahba)
	+~StagedWahbaSystemModel()
	+conv(const ObservationVector obs) : Eigen::Matrix<float , 7 , 1> {query}
	+h(const StateVector state) : Eigen::Matrix<float , 7 , 1> {query}
	+R(const StateVector state) : Eigen::Matrix<float , 7 , 7> {query}
	+H(const StateVector state) : Eigen::Matrix<float , 7 , N_state> {query}
	+F(const StateVector state) : Eigen::Matrix<float , N_state , N_state> {query}
	+Q(const StateVector state) : Eigen::Matrix<float , N_state , N_state> {query}
	-R_ : Eigen::Matrix<float, 7, 7>
	-Q_ : Eigen::Matrix<float, N_state, N_state>
	+f(const StateVector state) : StateVector {query}
	+f_noisy(const StateVector state) : StateVector {query}
	-wahba : WahbaEstimator&
	-dt : float
	+prob_obs(const StateVector state, Eigen::Matrix<float, 7, 1> obs) : float {query}
	-random_gen : mutable std::mt19937
	+name() : std::string {query}
}


abstract class StateEstimationFilter <template<int N_etat, int N_observation>> {
	+~StateEstimationFilter()
	+{abstract} Get_state() : Eigen::Matrix<float , N_etat , 1> {query}
	+{abstract} Predict() : Eigen::Matrix<float , N_etat , 1> {query}
	+{abstract} Update(const Eigen::Matrix<float, N_observation, 1> observation) : Eigen::Matrix<float , N_etat , 1>
	+{abstract} name() : std::string {query}
	+{abstract} Set_state(const Eigen::Matrix<float, N_etat, 1>) : void
}


abstract class SystemModel <template<int N_obs_interne>> {
	+~SystemModel()
	+{abstract} conv(const ObservationVector obs) : Eigen::Matrix<float , N_obs_interne , 1> {query}
	+{abstract} h(const StateVector state) : Eigen::Matrix<float , N_obs_interne , 1> {query}
	+{abstract} R(const StateVector state) : Eigen::Matrix<float , N_obs_interne , N_obs_interne> {query}
	+{abstract} H(const StateVector state) : Eigen::Matrix<float , N_obs_interne , N_state> {query}
	+{abstract} F(const StateVector state) : Eigen::Matrix<float , N_state , N_state> {query}
	+{abstract} Q(const StateVector state) : Eigen::Matrix<float , N_state , N_state> {query}
	+{abstract} f(const StateVector state) : StateVector {query}
	+{abstract} f_noisy(const StateVector state) : StateVector {query}
	+{abstract} prob_obs(const StateVector state, const Eigen::Matrix<float, N_obs_interne, 1> obs) : float {query}
	+{abstract} name() : std::string {query}
}


abstract class WahbaEstimator {
	+{abstract} estimate_q(const ObservationVector obs, const Eigen::Quaternionf first_guess) : Eigen::Quaternionf
	+estimate(const ObservationVector obs, Eigen::Quaternionf first_guess) : StateVector
	+{abstract} get_identifier() : std::string
}





/' Inheritance relationships '/

.AttitudeEstimator <|-- .ComplementayObserver


.AttitudeEstimator <|-- .FilterAttitudeEstimator


.AttitudeEstimator <|-- .PureWahbaAttitudeEstimator


.ObservationPreprocessor <|-- .FixedOffsetMagnetiquePreprocessor


.ObservationPreprocessor <|-- .OfflineMagnetiquePreprocessor


.StateEstimationFilter <|-- .GeneralKalmanFilter


.StateEstimationFilter <|-- .ParticleFilter


.SystemModel <|-- .NaiveSystemModel


.SystemModel <|-- .StagedWahbaSystemModel


.WahbaEstimator <|-- .LevenbergMarquardtWahba


.WahbaEstimator <|-- .QuestWahba





/' Aggregation relationships '/

.AttitudeEstimator o-- .ObservationPreprocessor


.ComplementayObserver o-- .WahbaEstimator


.FilterAttitudeEstimator *-- .StateEstimationFilter


.FilterAttitudeEstimator *-- .SystemModel


.GeneralKalmanFilter *-- .SystemModel


.ObservationPreprocessor o-- .ObservationPreprocessor


.ParticleFilter *-- .SystemModel


.PureWahbaAttitudeEstimator o-- .WahbaEstimator


.StagedWahbaSystemModel *-- .WahbaEstimator






/' Nested objects '/



@enduml
