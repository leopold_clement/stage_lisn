import csv
import numpy as np
import matplotlib.pyplot as plt

paths = [
         "data_out/Estimateur à filtre Kalman on model staged with Quest.csv",
         "data_out/Estimateur à filtre Kalman on naive model.csv",
         "data_out/Pure Quest.csv",
         "data_out/Estimateur à filtre Filtre particulaire de taille 10 on naive model.csv",
         "data_out/Estimateur à filtre Filtre particulaire de taille 20 on naive model.csv",
         "data_out/Estimateur à filtre Filtre particulaire de taille 50 on naive model.csv",
         "data_out/Estimateur à filtre Filtre particulaire de taille 10 on model staged with Quest.csv"
         ]


def get_file(path):
    with open(path) as file:
        reader = csv.reader(file)
        data = []
        for row in reader:
            try:
                data.append([float(x) for x in row])
            except Exception:
                pass
    return np.array(data)


fig, axs = plt.subplots(5, 1, sharex=True, sharey=False, figsize=(10, 10))
for path in paths:
    data = get_file(path)
    for idx in range(4):
        axs[idx].plot(data[:, idx], label=path, alpha = 0.5)
    axs[4].plot(np.sqrt(np.sum(data**2, axis=1)), label=path, alpha=0.5)

for ax in axs[:-1]:
    ax.set_ylim(-1.1, 1.1)
    ax.grid()
    ax.legend()
axs[4].grid()
axs[4].legend()

fig.tight_layout()
fig.savefig("out.png")
