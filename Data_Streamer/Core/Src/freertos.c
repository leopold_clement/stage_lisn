/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2023 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "semphr.h"
#include "usart.h"
#include "spi.h"
#include "StateVectors.h"
#include "IMU.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
typedef StaticTask_t osStaticThreadDef_t;
typedef StaticQueue_t osStaticMessageQDef_t;
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
uint32_t defaultTaskBuffer[ 128 ];
osStaticThreadDef_t defaultTaskControlBlock;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .cb_mem = &defaultTaskControlBlock,
  .cb_size = sizeof(defaultTaskControlBlock),
  .stack_mem = &defaultTaskBuffer[0],
  .stack_size = sizeof(defaultTaskBuffer),
  .priority = (osPriority_t) osPriorityLow1,
};
/* Definitions for IMU_monitoring */
osThreadId_t IMU_monitoringHandle;
uint32_t IMU_monitoringBuffer[ 512 ];
osStaticThreadDef_t IMU_monitoringControlBlock;
const osThreadAttr_t IMU_monitoring_attributes = {
  .name = "IMU_monitoring",
  .cb_mem = &IMU_monitoringControlBlock,
  .cb_size = sizeof(IMU_monitoringControlBlock),
  .stack_mem = &IMU_monitoringBuffer[0],
  .stack_size = sizeof(IMU_monitoringBuffer),
  .priority = (osPriority_t) osPriorityHigh,
};
/* Definitions for IMU_data_sender */
osThreadId_t IMU_data_senderHandle;
uint32_t IMU_data_senderBuffer[ 512 ];
osStaticThreadDef_t IMU_data_senderControlBlock;
const osThreadAttr_t IMU_data_sender_attributes = {
  .name = "IMU_data_sender",
  .cb_mem = &IMU_data_senderControlBlock,
  .cb_size = sizeof(IMU_data_senderControlBlock),
  .stack_mem = &IMU_data_senderBuffer[0],
  .stack_size = sizeof(IMU_data_senderBuffer),
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for Xsens_monitoring */
osThreadId_t Xsens_monitoringHandle;
uint32_t Xsens_monitoringBuffer[ 512 ];
osStaticThreadDef_t Xsens_monitoringControlBlock;
const osThreadAttr_t Xsens_monitoring_attributes = {
  .name = "Xsens_monitoring",
  .cb_mem = &Xsens_monitoringControlBlock,
  .cb_size = sizeof(Xsens_monitoringControlBlock),
  .stack_mem = &Xsens_monitoringBuffer[0],
  .stack_size = sizeof(Xsens_monitoringBuffer),
  .priority = (osPriority_t) osPriorityHigh,
};
/* Definitions for Xsens_data_sender */
osThreadId_t Xsens_data_senderHandle;
uint32_t Xsens_data_senderBuffer[ 512 ];
osStaticThreadDef_t Xsens_data_senderControlBlock;
const osThreadAttr_t Xsens_data_sender_attributes = {
  .name = "Xsens_data_sender",
  .cb_mem = &Xsens_data_senderControlBlock,
  .cb_size = sizeof(Xsens_data_senderControlBlock),
  .stack_mem = &Xsens_data_senderBuffer[0],
  .stack_size = sizeof(Xsens_data_senderBuffer),
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for IMU_data */
osMessageQueueId_t IMU_dataHandle;
const osMessageQueueAttr_t IMU_data_attributes = {
  .name = "IMU_data"
};
/* Definitions for Xsens_data */
osMessageQueueId_t Xsens_dataHandle;
const osMessageQueueAttr_t Xsens_data_attributes = {
  .name = "Xsens_data"
};
/* Definitions for DebugMID */
osMessageQueueId_t DebugMIDHandle;
uint8_t DebugMIDBuffer[ 16 * sizeof( uint8_t ) ];
osStaticMessageQDef_t DebugMIDControlBlock;
const osMessageQueueAttr_t DebugMID_attributes = {
  .name = "DebugMID",
  .cb_mem = &DebugMIDControlBlock,
  .cb_size = sizeof(DebugMIDControlBlock),
  .mq_mem = &DebugMIDBuffer,
  .mq_size = sizeof(DebugMIDBuffer)
};
/* Definitions for SPI_bus_mutex */
osMutexId_t SPI_bus_mutexHandle;
const osMutexAttr_t SPI_bus_mutex_attributes = {
  .name = "SPI_bus_mutex"
};
/* Definitions for UART_mutex */
osMutexId_t UART_mutexHandle;
const osMutexAttr_t UART_mutex_attributes = {
  .name = "UART_mutex"
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);
void startIMU_monitoring(void *argument);
void StartIMUData_sender(void *argument);
void StartXsens_monitoring(void *argument);
void StartXsens_sata_sender(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);

/* USER CODE BEGIN 1 */
volatile unsigned long perf_counter;
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
void configureTimerForRunTimeStats(void) {
  perf_counter = 0;
}

unsigned long getRunTimeCounterValue(void) {
	return perf_counter;
}
/* USER CODE END 1 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */
  /* Create the mutex(es) */
  /* creation of SPI_bus_mutex */
  SPI_bus_mutexHandle = osMutexNew(&SPI_bus_mutex_attributes);

  /* creation of UART_mutex */
  UART_mutexHandle = osMutexNew(&UART_mutex_attributes);

  /* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* creation of IMU_data */
  IMU_dataHandle = osMessageQueueNew (1, sizeof(FullMesureVector_f), &IMU_data_attributes);

  /* creation of Xsens_data */
  Xsens_dataHandle = osMessageQueueNew (1, sizeof(uint16_t), &Xsens_data_attributes);

  /* creation of DebugMID */
  DebugMIDHandle = osMessageQueueNew (16, sizeof(uint8_t), &DebugMID_attributes);

  /* USER CODE BEGIN RTOS_QUEUES */
	/* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* creation of IMU_monitoring */
  IMU_monitoringHandle = osThreadNew(startIMU_monitoring, NULL, &IMU_monitoring_attributes);

  /* creation of IMU_data_sender */
  IMU_data_senderHandle = osThreadNew(StartIMUData_sender, NULL, &IMU_data_sender_attributes);

  /* creation of Xsens_monitoring */
  Xsens_monitoringHandle = osThreadNew(StartXsens_monitoring, NULL, &Xsens_monitoring_attributes);

  /* creation of Xsens_data_sender */
  Xsens_data_senderHandle = osThreadNew(StartXsens_sata_sender, NULL, &Xsens_data_sender_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
	/* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
 * @brief  Function implementing the defaultTask thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN StartDefaultTask */
	TickType_t xLastWakeTime;
	/* Infinite loop */
	for (;;) {
		vTaskSuspend(0);
		vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(1000));

	}
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_startIMU_monitoring */
/**
 * @brief Function implementing the IMU_monitoring thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_startIMU_monitoring */
void startIMU_monitoring(void *argument)
{
  /* USER CODE BEGIN startIMU_monitoring */
	/* Infinite loop */
	FullMesureVector_f mesure;
	TickType_t xLastWakeTime;

	xSemaphoreTake(SPI_bus_mutexHandle, portMAX_DELAY);
	IMU_ICM20948_init_imu();
	xSemaphoreGive(SPI_bus_mutexHandle);

	xLastWakeTime = xTaskGetTickCount();
	for (;;) {

		xSemaphoreTake(SPI_bus_mutexHandle, portMAX_DELAY);
		HAL_StatusTypeDef SPI_responce_state = IMU_ICM20948_read9D_f(&mesure);
		xSemaphoreGive(SPI_bus_mutexHandle);

		if (SPI_responce_state == HAL_OK) {
			xQueueSend(IMU_dataHandle, &mesure, 0);
		}

		vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(50));
	}
  /* USER CODE END startIMU_monitoring */
}

/* USER CODE BEGIN Header_StartIMUData_sender */
/**
 * @brief Function implementing the IMU_data_sender thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartIMUData_sender */
void StartIMUData_sender(void *argument)
{
  /* USER CODE BEGIN StartIMUData_sender */
	/* Infinite loop */
	FullMesureVector_f mesure_IMU;
	unsigned char buffer_imu_uart[255];
	for (;;) {

		if (xQueueReceive(IMU_dataHandle, &mesure_IMU,
				pdMS_TO_TICKS(10)) == pdPASS) {
			int len = format_FullMesureVector_f( (char*) buffer_imu_uart,	mesure_IMU);
			xSemaphoreTake(UART_mutexHandle, portMAX_DELAY);
			// HAL_UART_Transmit(&huart2, "IMU : ", 6, HAL_MAX_DELAY);
			HAL_UART_Transmit(&huart2, buffer_imu_uart, len, HAL_MAX_DELAY);
			HAL_UART_Transmit(&huart2, "\n", 1, HAL_MAX_DELAY);
			xSemaphoreGive(UART_mutexHandle);
		}
	}
  /* USER CODE END StartIMUData_sender */
}

/* USER CODE BEGIN Header_StartXsens_monitoring */
/**
* @brief Function implementing the Xsens_monitoring thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartXsens_monitoring */
void StartXsens_monitoring(void *argument)
{
  /* USER CODE BEGIN StartXsens_monitoring */
  /* Infinite loop */
	StateVector state;
	TickType_t xLastWakeTime;
	vTaskSuspend(NULL);
	/*
	xSemaphoreTake(I2C_bus_mutexHandle, portMAX_DELAY);
	IMU_Xsens_init_xsens1();
	IMU_Xsens_init(&xsens1_h);
	xSemaphoreGive(I2C_bus_mutexHandle);

	xLastWakeTime = xTaskGetTickCount();
	for (;;) {

		xSemaphoreTake(I2C_bus_mutexHandle, portMAX_DELAY);
		HAL_StatusTypeDef I2C_responce_state = IMU_Xsens_readState(&xsens1_h,
				&state);
		xSemaphoreGive(I2C_bus_mutexHandle);

		if (I2C_responce_state == HAL_OK) {
			xQueueSend(Xsens_dataHandle, &state, 0);
		}

		vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(50));
	}
	*/
  /* USER CODE END StartXsens_monitoring */
}

/* USER CODE BEGIN Header_StartXsens_sata_sender */
/**
* @brief Function implementing the Xsens_data_sender thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartXsens_sata_sender */
void StartXsens_sata_sender(void *argument)
{
  /* USER CODE BEGIN StartXsens_sata_sender */
  /* Infinite loop */
	StateVector state;
  uint8_t mid;
	unsigned char buffer_imu_uart[255];
	for (;;) {

		if (xQueueReceive(Xsens_dataHandle, &state,
				pdMS_TO_TICKS(10)) == pdPASS) {
			int len = format_StateVector(state, buffer_imu_uart);
			xSemaphoreTake(UART_mutexHandle, portMAX_DELAY);
			HAL_UART_Transmit(&huart2, "Xsens : ", 8, HAL_MAX_DELAY);
			HAL_UART_Transmit(&huart2, buffer_imu_uart, len, HAL_MAX_DELAY);
			HAL_UART_Transmit(&huart2, "\n", 1, HAL_MAX_DELAY);
			xSemaphoreGive(UART_mutexHandle);
		}
    if (xQueueReceive(DebugMIDHandle, &mid,
				pdMS_TO_TICKS(10)) == pdPASS) {
			int len = sprintf(buffer_imu_uart, "%u", mid);
			xSemaphoreTake(UART_mutexHandle, portMAX_DELAY);
			HAL_UART_Transmit(&huart2, "DebugMID : ", 11, HAL_MAX_DELAY);
			HAL_UART_Transmit(&huart2, buffer_imu_uart, len, HAL_MAX_DELAY);
			HAL_UART_Transmit(&huart2, "\n", 1, HAL_MAX_DELAY);
			xSemaphoreGive(UART_mutexHandle);
		}
	}
  /* USER CODE END StartXsens_sata_sender */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

