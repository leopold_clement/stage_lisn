/*
 * StateVectors.c
 *
 *  Created on: Apr 19, 2023
 *      Author: leopold
 */

#include "StateVectors.h"
#include "Mesure.h"
uint16_t format_StateVector(StateVector state, uint8_t* str){
	int len = format_MesureVector_f(state.omega, str);
	str[len] = ',';
	len++;
	len += format_Quaternion_f(state.q, str+len);
	return len;
}


