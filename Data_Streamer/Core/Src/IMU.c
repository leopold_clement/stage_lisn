//#include "imc20948.h"
#include "imc20948.h"
#include "spi.h"
#include "IMU.h"
#include "stdlib.h"
#include "stdio.h"
#include "stdint.h"

/**
 * Read value from
 */
HAL_StatusTypeDef IMU_ICM20948_read9D_f(FullMesureVector_f *mesure) {
	icm20948_accel_read_g(&(mesure->acc));
	icm20948_gyro_read_dps(&(mesure->gyro));
	ak09916_mag_read_uT(&(mesure->mag));

	return HAL_OK;
}


HAL_StatusTypeDef IMU_ICM20948_init_imu(void){
    icm20948_init();
    ak09916_init();
	return HAL_OK;
}
