/*
 * Quaternions.c
 *
 *  Created on: Apr 19, 2023
 *      Author: leopold
 */
#include "Quaternions.h"
#include <stdio.h>

int format_Quaternion_f(Quaternion_f q, uint8_t *str){
	return sprintf((char*)str, "(%f + %f i + %f j + %f k)", q.w, q.i, q.j, q.w);
}
