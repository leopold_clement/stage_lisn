/*
 * Mesure.c
 *
 *  Created on: Apr 19, 2023
 *      Author: leopold
 */
#include "Mesure.h"
#include <stdio.h>

int format_MesureVector_f(MesureVector_f mesure, uint8_t *str ){
	return sprintf((char *)str, "(%f, %f, %f)", mesure.x, mesure.y, mesure.z);
}

uint16_t format_FullMesureVector_i(char *str, FullMesureVector_i vec) {
	return sprintf(str, "%i, %i, %i, %i, %i, %i, %i, %i, %i", vec.acc.x,
			vec.acc.y, vec.acc.z, vec.gyro.x, vec.gyro.y, vec.gyro.z, vec.mag.x,
			vec.mag.y, vec.mag.z);
}

uint16_t format_FullMesureVector_f(char *str, FullMesureVector_f vec) {
	return sprintf(str, "%f, %f, %f, %f, %f, %f, %f, %f, %f", vec.acc.x,
			vec.acc.y, vec.acc.z, vec.gyro.x, vec.gyro.y, vec.gyro.z, vec.mag.x,
			vec.mag.y, vec.mag.z);
}