/*
 * Quaternions.h
 *
 *  Created on: Apr 19, 2023
 *      Author: leopold
 */

#ifndef INC_QUATERNIONS_H_
#define INC_QUATERNIONS_H_

#include "stdint.h"

typedef struct {
	float w;
	float i;
	float j;
	float k;
} Quaternion_f;

typedef struct {
	int16_t w;
	int16_t i;
	int16_t j;
	int16_t k;
} Quaternion_i;

int format_Quaternion_f(Quaternion_f q, uint8_t *str);

#endif /* INC_QUATERNIONS_H_ */
