/*
 * StateVector.h
 *
 *  Created on: Apr 19, 2023
 *      Author: leopold
 */

#ifndef SRC_STATEVECTORS_H_
#define SRC_STATEVECTORS_H_

#include "Mesure.h"
#include "Quaternions.h"
#include "stdint.h"

typedef struct{
	Quaternion_f q;
	MesureVector_f omega;
}StateVector;

uint16_t format_StateVector(StateVector state, uint8_t* str);

#endif /* SRC_STATEVECTOR_H_ */
