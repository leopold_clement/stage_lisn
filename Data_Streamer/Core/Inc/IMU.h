#ifndef __IMU_H__
#define	__IMU_H__

#include "spi.h"
#include "Mesure.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Initialise l'IMU
 * 
 * @param imu 
 * @return HAL_StatusTypeDef 
 */
HAL_StatusTypeDef IMU_ICM20948_init_imu(void);

/**
 * @brief Lis un point de mesure et le pkace dans `mesure`.
 * 
 * @param imu 
 * @param mesure 
 * @return HAL_StatusTypeDef 
 */
HAL_StatusTypeDef IMU_ICM20948_read9D_f(FullMesureVector_f *mesure);

#ifdef __cplusplus
}
#endif


#endif
