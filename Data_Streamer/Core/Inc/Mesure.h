#ifndef __MESURE_H__
#define __MESURE_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "stdint.h"

	/**
	 * @brief Structure représentant un vecteur 3D
	 *
	 */
	typedef struct MesureVector_i
	{
		int16_t x;
		int16_t y;
		int16_t z;
	} MesureVector_i;
	typedef struct MesureVector_f
	{
		float x;
		float y;
		float z;
	} MesureVector_f;

	int format_MesureVector_i(MesureVector_i mesure, uint8_t *str);
	int format_MesureVector_f(MesureVector_f mesure, uint8_t *str);

	/**
	 * @brief Structure représentan tun point de mesure avec 3 vecteurs
	 *
	 */
	typedef struct FullMesureVector_i
	{
		MesureVector_i acc;
		MesureVector_i gyro;
		MesureVector_i mag;
		uint8_t mag_sat;
	} FullMesureVector_i;

	typedef struct FullMesureVector_f
	{
		MesureVector_f acc;
		MesureVector_f gyro;
		MesureVector_f mag;
		uint8_t mag_sat;
	} FullMesureVector_f;

	/**
	 * @brief Représente un point de mesure par une chaine de caractère
	 *
	 * @param str
	 * @param vec
	 * @return uint16_t
	 */
	uint16_t format_FullMesureVector_i(char *str, FullMesureVector_i vec);
	uint16_t format_FullMesureVector_f(char *str, FullMesureVector_f vec);

#ifdef __cplusplus
}
#endif

#endif
