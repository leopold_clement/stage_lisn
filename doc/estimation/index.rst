Estimation of the device rotation
=================================



Notre objectif est de réaliser la classe suivante :

.. autoclass:: vestelisn_proto.estimation_local.GenericEstimator.Estimator9DOF
    :members:

.. toctree::
   wahba
   kalman
   co
   :maxdepth: 2
   :caption: Contents: