Filtre de Kalman
================

.. inheritance-diagram:: vestelisn_proto.estimation_local.Kalman.ReducedKalmanFilter vestelisn_proto.estimation_local.Kalman.NaiveKalmanFilter
    :parts: 1

.. automodule:: vestelisn_proto.estimation_local.Kalman
    :members: