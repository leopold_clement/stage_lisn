.. Stage veste connectée documentation master file, created by
   sphinx-quickstart on Thu Mar 16 15:04:21 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Stage veste connectée's documentation!
=================================================

.. toctree::
   estimation/index
   quaternions
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
